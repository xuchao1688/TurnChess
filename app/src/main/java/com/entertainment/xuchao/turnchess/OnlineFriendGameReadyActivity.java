package com.entertainment.xuchao.turnchess;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.entertainment.xuchao.turnchess.socket.*;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import entity.*;

/**
 * Created by Administrator1 on 2017/4/19.
 */

public class OnlineFriendGameReadyActivity extends AppCompatActivity{
    private Button backToMainBtn;
    private Button addNewFriendBtn;
    private Button friendGamePlaceBtn;
    private ListView friendListView;
    private List<Map<String, Object>> mData;
    private FriendListAdapter mAdapter;
    private View mProgressView;
    private View mFriendGameReadyFormView;
    private boolean busy;
    private int oppoID;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friendgame);
        initView();
    }
    private void initView(){
        InitializeConnection.getInitConn();
        busy = false;
        mData = new ArrayList<Map<String,Object>>();
        ResponseHandler.getResHdler().setResponseListener(new ResponseHandler.ResponseListener() {
            @Override
            public void doevent(int status) {
                if(status == ResponseObject.RES_LOADFRIENDLIST){
                    mData.clear();
                    System.out.println((LoadfriendlistInfoRes)ResponseHandler.getResHdler().getResObject().getResBody());
                    int[] userlist = ((LoadfriendlistInfoRes)ResponseHandler.getResHdler().getResObject().getResBody()).getUserlist();
                    String[] username = ((LoadfriendlistInfoRes)ResponseHandler.getResHdler().getResObject().getResBody()).getUsername();
                    int[] useronline = ((LoadfriendlistInfoRes)ResponseHandler.getResHdler().getResObject().getResBody()).getUseronline();
                    int[] userbattle = ((LoadfriendlistInfoRes)ResponseHandler.getResHdler().getResObject().getResBody()).getUserbattle();
                    for(int i = 0; i < userlist.length; i++){
                        Map<String,Object> map = new HashMap<String,Object>();
                        map.put("username",username[i]);
                        map.put("status",userbattle[i] == 1 ? "busy":(useronline[i] == 1 ? "online" : "offline"));
                        map.put("id",userlist[i]);
                        mData.add(map);
                    }
                }
            }
        });
        new Thread(new Runnable() {
            @Override
            public void run() {
                RequestHandler.getReqHdler().sendMessage(new RequestObject(RequestObject.REQ_LOADFRIENDLIST,new LoadfriendlistInfo()));
                ResponseHandler.getResHdler().readMessage();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mAdapter.notifyDataSetChanged();
                    }
                });
            }
        }).start();
        backToMainBtn = (Button)findViewById(R.id.backToMainBtn);
        backToMainBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        addNewFriendBtn = (Button)findViewById(R.id.addNewFriendBtn);
        addNewFriendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AlertDialog.Builder alertdialogBuilder = new AlertDialog.Builder(OnlineFriendGameReadyActivity.this);
                final EditText mtext = new EditText(OnlineFriendGameReadyActivity.this);
                alertdialogBuilder.setTitle(R.string.input_add_newfriend).setView(mtext)
                        .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(final DialogInterface dialog, int which) {
                                boolean cancel = true;
                                String errmsg = null;
                                if (TextUtils.isEmpty(mtext.getText().toString())) {
                                    errmsg = getString(R.string.error_field_required);
                                    cancel = false;
                                } else if (!isAccountValid(mtext.getText().toString())) {
                                    errmsg = getString(R.string.error_invalid_account);
                                    cancel = false;
                                }
                                if(!cancel){
                                    AlertDialog.Builder alertdialogBuilder2 = new AlertDialog.Builder(OnlineFriendGameReadyActivity.this);
                                    alertdialogBuilder2.setTitle(R.string.sorry).setMessage(errmsg).setPositiveButton(getString(R.string.yes),null);
                                    AlertDialog alertDialog2 = alertdialogBuilder2.create();
                                    alertDialog2.setCanceledOnTouchOutside(false);
                                    alertDialog2.show();
                                    return;
                                }
                                new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        RequestHandler.getReqHdler().sendMessage(new RequestObject(RequestObject.REQ_ADDNEWFRIEND,new NewFriendInfo(mtext.getText().toString())));
                                        ResponseHandler.getResHdler().setResponseListener(new ResponseHandler.ResponseListener() {
                                            @Override
                                            public void doevent(int status) {
                                                if(status == ResponseObject.RES_ADDNEWFRIEND){
                                                    runOnUiThread(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            NewFriendInfoRes newFriendInfoRes = (NewFriendInfoRes)ResponseHandler.getResHdler().getResObject().getResBody();
                                                            String title = null, mess = null;
                                                            if(newFriendInfoRes.getMessage().equals("fail")) {
                                                                title = getString(R.string.sorry);
                                                                mess = getString(R.string.error_notfound_account);
                                                            }else if(newFriendInfoRes.getMessage().equals("duplicate")){
                                                                title = getString(R.string.sorry);
                                                                mess = getString(R.string.duplicate_newfriendrequest);
                                                            }else{
                                                                title = getString(R.string.congratulation);
                                                                mess = getString(R.string.success_send_newfriendrequest);
                                                            }
                                                            AlertDialog.Builder alertdialogBuilder2 = new AlertDialog.Builder(OnlineFriendGameReadyActivity.this);
                                                            alertdialogBuilder2.setTitle(title).setMessage(mess).setPositiveButton(getString(R.string.yes),null);
                                                            AlertDialog alertDialog2 = alertdialogBuilder2.create();
                                                            alertDialog2.setCanceledOnTouchOutside(false);
                                                            alertDialog2.show();
                                                        }
                                                    });
                                                }
                                            }
                                        });
                                        ResponseHandler.getResHdler().readMessage();
                                    }
                                }).start();
                            }
                        })
                        .setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                AlertDialog alertDialog = alertdialogBuilder.create();
                alertDialog.setCanceledOnTouchOutside(false);
                alertDialog.show();
            }
        });
        friendGamePlaceBtn =(Button) findViewById(R.id.friendGamePlaceBtn);
        friendGamePlaceBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(OnlineFriendGameReadyActivity.this,OnlineFriendGamePlaceActivity.class);
                startActivity(intent);
            }
        });
        friendListView = (ListView)findViewById(R.id.friendlist);
        friendListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        RequestHandler.getReqHdler().sendMessage(new RequestObject(RequestObject.REQ_LOADUSERINFO,new LoadUserInfo(mData.get(position).get("username").toString(),false)));
                        ResponseHandler.getResHdler().setResponseListener(new ResponseHandler.ResponseListener() {
                            @Override
                            public void doevent(int status) {
                                if(status == ResponseObject.RES_LOADUSERINFO){
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            LoadUserInfoRes loadUserInfoRes = (LoadUserInfoRes)ResponseHandler.getResHdler().getResObject().getResBody();
                                            DecimalFormat decimalFormat=new DecimalFormat(".00%");
                                            AlertDialog.Builder alertdialogBuilder = new AlertDialog.Builder(OnlineFriendGameReadyActivity.this);
                                            alertdialogBuilder.setTitle(R.string.friend_userinfo)
                                                    .setMessage((String)(loadUserInfoRes.getUsername() + "\n" + getString(R.string.win)+
                                                    ":  " + loadUserInfoRes.getWin() + "\n" + getString(R.string.lose))+
                                                    ":  " + loadUserInfoRes.getLose() + "\n" + getString(R.string.win_rate)+
                                                    ":  " + decimalFormat.format(1.00f * loadUserInfoRes.getWin()/(loadUserInfoRes.getWin()+loadUserInfoRes.getLose())))
                                                    .setPositiveButton(getString(R.string.yes), null)
                                                    .setNegativeButton(getString(R.string.delete_friend), new DialogInterface.OnClickListener() {
                                                        @Override
                                                        public void onClick(DialogInterface dialog, int which) {
                                                            new Thread(new Runnable() {
                                                                @Override
                                                                public void run() {
                                                                    RequestHandler.getReqHdler().sendMessage(new RequestObject(RequestObject.REQ_DELETEFRIEND,new NewFriendInfo(mData.get(position).get("username").toString())));
                                                                    ResponseHandler.getResHdler().readMessage();
                                                                    runOnUiThread(new Runnable() {
                                                                        @Override
                                                                        public void run() {
                                                                            mAdapter.notifyDataSetChanged();
                                                                        }
                                                                    });
                                                                }
                                                            }).start();
                                                        }
                                                    });
                                            AlertDialog alertDialog = alertdialogBuilder.create();
                                            alertDialog.setCanceledOnTouchOutside(false);
                                            alertDialog.show();
                                        }
                                    });
                                }else if(status == ResponseObject.RES_LOADFRIENDLIST){
                                    mData.clear();
                                    System.out.println((LoadfriendlistInfoRes)ResponseHandler.getResHdler().getResObject().getResBody());
                                    int[] userlist = ((LoadfriendlistInfoRes)ResponseHandler.getResHdler().getResObject().getResBody()).getUserlist();
                                    String[] username = ((LoadfriendlistInfoRes)ResponseHandler.getResHdler().getResObject().getResBody()).getUsername();
                                    int[] useronline = ((LoadfriendlistInfoRes)ResponseHandler.getResHdler().getResObject().getResBody()).getUseronline();
                                    int[] userbattle = ((LoadfriendlistInfoRes)ResponseHandler.getResHdler().getResObject().getResBody()).getUserbattle();
                                    for(int i = 0; i < userlist.length; i++){
                                        Map<String,Object> map = new HashMap<String,Object>();
                                        map.put("username",username[i]);
                                        map.put("status",userbattle[i] == 1 ? "busy":(useronline[i] == 1 ? "online" : "offline"));
                                        map.put("id",userlist[i]);
                                        mData.add(map);
                                    }
                                }
                            }
                        });
                        ResponseHandler.getResHdler().readMessage();
                    }
                }).start();
            }
        });
        mAdapter = new FriendListAdapter(this);
        friendListView.setAdapter(mAdapter);
        mProgressView = findViewById(R.id.friendgame_progress);
        mFriendGameReadyFormView = findViewById(R.id.friendgame_form);
    }

    public final class ViewHolder{
        public TextView friendlist_username;
        public TextView friendlist_status;
        public Button playWithFriendBtn;
    }

    public class FriendListAdapter extends BaseAdapter{
        private LayoutInflater mInflater;
        public FriendListAdapter(Context context){
            this.mInflater = LayoutInflater.from(context);
        }

        @Override
        public int getCount(){
            return mData.size();
        }

        @Override
        public Object getItem(int arg0) {
            return null;
        }

        @Override
        public long getItemId(int arg0) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent){
            ViewHolder holder = null;
            if(convertView == null){
                holder = new ViewHolder();
                convertView = mInflater.inflate(R.layout.friendlist,null);
                holder.friendlist_username = (TextView)convertView.findViewById(R.id.friendlist_username);
                holder.friendlist_status = (TextView)convertView.findViewById(R.id.friendlist_status);
                holder.playWithFriendBtn = (Button) convertView.findViewById(R.id.playWithFriendBtn);
                convertView.setTag(holder);
            }else {
                holder = (ViewHolder)convertView.getTag();
            }
            holder.friendlist_username.setText((String)mData.get(position).get("username"));
            holder.friendlist_status.setText((String)mData.get(position).get("status"));
            if(mData.get(position).get("status").toString().equals("busy") || mData.get(position).get("status").toString().equals("offline")){
                holder.playWithFriendBtn.setVisibility(View.GONE);
            }
            final String name = (String)mData.get(position).get("username");
            final int id = (int)mData.get(position).get("id");
            holder.playWithFriendBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    busy = true;
                    oppoID = id;
                    showProgress(true);
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            ResponseHandler.getResHdler().setResponseListener(new ResponseHandler.ResponseListener() {
                                @Override
                                public void doevent(int status) {
                                    if(status == ResponseObject.RES_FRIENDGAME){
                                        final FriendGameInfoRes friendGameInfoRes = (FriendGameInfoRes)ResponseHandler.getResHdler().getResObject().getResBody();
                                        if(friendGameInfoRes.getGameType() == FriendGameInfoRes.GAME_START){
                                            runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    showProgress(false);
                                                    busy = false;
                                                }
                                            });
                                            Intent intent = new Intent(OnlineFriendGameReadyActivity.this,OnlineGameActivity.class);
                                            startActivity(intent);
                                        }else if(friendGameInfoRes.getGameType() == FriendGameInfoRes.GAME_CANCEL){
                                            runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    showProgress(false);
                                                    busy = false;
                                                    AlertDialog.Builder alertdialogBuilder = new AlertDialog.Builder(OnlineFriendGameReadyActivity.this);
                                                    alertdialogBuilder.setTitle(R.string.self_userinfo)
                                                            .setMessage(getString(R.string.refuse_your_invite))
                                                            .setPositiveButton(getString(R.string.yes), null);
                                                    AlertDialog alertDialog = alertdialogBuilder.create();
                                                    alertDialog.setCanceledOnTouchOutside(false);
                                                    alertDialog.show();
                                                }
                                            });
                                        }
                                    }
                                }
                            });
                            FriendGameInfo friendGameInfo = new FriendGameInfo(FriendGameInfo.GAME_READY);
                            friendGameInfo.setUid(oppoID);
                            RequestHandler.getReqHdler().sendMessage(new RequestObject(RequestObject.REQ_FRIENDGAME,friendGameInfo));
                            ResponseHandler.getResHdler().readMessage();
                        }
                    }).start();
                }
            });
            return convertView;
        }
    }
    private boolean isAccountValid(String account) {
        boolean isDigit = false;
        boolean isLetter = false;
        for(int i = 0; i < account.length();i++){
            if(Character.isDigit(account.charAt(i))){
                isDigit = true;
            }else if (Character.isLetter(account.charAt(i))){
                isLetter = true;
            }
        }
        return isDigit && isLetter && (account.length() >= 4);
    }

    @Override
    public void onBackPressed(){
        if(busy){
            new Thread(new Runnable() {
                @Override
                public void run() {
                    FriendGameInfo friendGameInfo = new FriendGameInfo(FriendGameInfo.GAME_CACNCEL);
                    friendGameInfo.setUid(oppoID);
                    RequestHandler.getReqHdler().sendMessage(new RequestObject(RequestObject.REQ_FRIENDGAME,friendGameInfo));
                }
            }).start();
        }
        super.onBackPressed();
    }
    /**
     * Shows the progress UI and hides the online main form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mFriendGameReadyFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mFriendGameReadyFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mFriendGameReadyFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mFriendGameReadyFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }
}
