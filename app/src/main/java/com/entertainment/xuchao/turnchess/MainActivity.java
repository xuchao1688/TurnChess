package com.entertainment.xuchao.turnchess;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    private Button localGameStartBtn;
    private Button onlineGameStartBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
    }

    private void initView(){
        localGameStartBtn = (Button)findViewById(R.id.localGameStartBtn);
        localGameStartBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,LocalGameActivity.class);
                startActivity(intent);
            }
        });
        onlineGameStartBtn = (Button)findViewById(R.id.onlineGameStartBtn);
        onlineGameStartBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,OnlineGameLoginActivity.class);
                startActivity(intent);
            }
        });
    }
}
