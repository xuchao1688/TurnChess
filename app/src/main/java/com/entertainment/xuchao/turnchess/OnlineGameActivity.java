package com.entertainment.xuchao.turnchess;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;

import com.entertainment.xuchao.turnchess.socket.InitializeConnection;
import com.entertainment.xuchao.turnchess.socket.RequestHandler;

import entity.FriendGameInfo;
import entity.MatchGameInfo;
import entity.RequestObject;
import entity.ResponseObject;

/**
 * Created by Administrator1 on 2017/4/22.
 */

public class OnlineGameActivity extends AppCompatActivity {
    private TurnchessOnlineGameView turnchessOnlineGameView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        turnchessOnlineGameView = new TurnchessOnlineGameView(this.getBaseContext());
        turnchessOnlineGameView.setOnGameEndListener(new TurnchessOnlineGameView.OnGameEndListener() {
            @Override
            public void onGameEnd(final int status) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        AlertDialog.Builder alertdialogBuilder = new AlertDialog.Builder(OnlineGameActivity.this);
                        alertdialogBuilder.setTitle(R.string.game_over)
                                .setMessage(status == 1 ? R.string.you_win : R.string.you_lose)
                                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        new Thread(new Runnable() {
                                            @Override
                                            public void run() {
                                                MatchGameInfo matchGameInfo = new MatchGameInfo(MatchGameInfo.GAME_END);
                                                matchGameInfo.setSelfWin(status == 1);
                                                RequestHandler.getReqHdler().sendMessage(new RequestObject(RequestObject.REQ_MATCHGAME,matchGameInfo));
                                            }
                                        }).start();
                                        dialog.dismiss();
                                        finish();
                                    }
                                });
                        AlertDialog alertDialog = alertdialogBuilder.create();
                        alertDialog.setCanceledOnTouchOutside(false);
                        alertDialog.show();
                    }
                });
            }
        });
        setContentView(turnchessOnlineGameView);
    }
    @Override
    public void onBackPressed(){
        AlertDialog.Builder alertdialogBuilder = new AlertDialog.Builder(OnlineGameActivity.this);
        alertdialogBuilder.setTitle(R.string.quit)
                .setMessage(R.string.sure_leave_game)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                InitializeConnection.getInitConn().reConnect();
                            }
                        }).start();
                        dialog.dismiss();
                        finish();
                    }
                })
                .setNegativeButton(R.string.cancel,null);
        AlertDialog alertDialog = alertdialogBuilder.create();
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.show();
    }
}