package com.entertainment.xuchao.turnchess.socket;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;

/**
 * Created by Administrator1 on 2017/3/26.
 */

public class InitializeConnection {
    private Socket socket;
    private OutputStream os;
    private InputStream is ;
    private ObjectOutputStream oos ;
    private ObjectInputStream ois;
    private static InitializeConnection initConn = null;
    public static InitializeConnection getInitConn(){
        if(initConn == null)
            initConn = new InitializeConnection();
        return  initConn;
    }
    public InitializeConnection(){
        initConn();
    }

    public void initConn(){
        try{
//            if(socket != null){
//                return;
//            }
            socket = new Socket("chat.isdas.cn",2333);  //establish TCP connection to server
            os = socket.getOutputStream();
            is = socket.getInputStream();
            oos = new ObjectOutputStream(os);
            ois = new ObjectInputStream(is);
            RequestHandler.getReqHdler().setObjectOutputStream(oos);
            ResponseHandler.getResHdler().setObjectInputStream(ois);
        }catch (Exception e){
            e.printStackTrace();
            reConnect();
        }
    }

    public void stopConnect(){
        try{
            ois.close();
            oos.close();
            os.close();
            is.close();
            socket.close();
            socket = null;
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    public void reConnect(){
        stopConnect();
        initConn();
    }

}
