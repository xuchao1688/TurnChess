package com.entertainment.xuchao.turnchess;

/**
 * Created by Administrator1 on 2017/4/22.
 */

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.entertainment.xuchao.turnchess.socket.RequestHandler;
import com.entertainment.xuchao.turnchess.socket.ResponseHandler;

import java.util.Date;
import java.util.Random;

import entity.*;

public class TurnchessOnlineGameView extends SurfaceView implements SurfaceHolder.Callback{
    private SurfaceHolder holder;
    private boolean isDraw = false;
    private RenderThread renderThread;
    private float ratio,ratio2;
    private int defaultWidth = 1080,defaultHeight = 1776;
    private Paint paintChessboard,paintChess,paintChessBackground,paintChessWrd,paintStaticsBackgroud;
    private Bitmap bmpChessboard;
    private int chessboard_sX,chessboard_sY,chess_sX,chess_sY;
    private int chessWidth,chessHeight;
    private int chessStatics_sX,chessStatics_sY_Enemy,chessStatics_sY;
    private int chessStaticsWidth;
    private int horizonInterval,verticalInterval,bigVerticalInterval;
    private int staticsHorizonInterval, staticsVerticalInterval;
    private int action_sX,action_sY_Enemy,action_sY;
    private int userInfo_sX,userInfo_sY;
    private int userInfo2_sX,userInfo2_sY;
    private Chess[][] chessboard;
    private int[][][] chessStaticsUI;
    private int[][][] chessStatics;
    private int whoIsEnemy;           //0: blue 1: red
    private int state; //-1:not ensure who is blue or red 0: normal 1:chess selected
    private boolean isDownBlue; //false: the down side is red
    private int[][] turnStatics;
    private int[][] gotoChessboard;
    private int opposeID;
    private int oldI,oldJ,newI,newJ;
    private boolean canGo;
    private boolean isMatchGame;
    private String opposeAccount;
    private String opposeWinRate;
    public TurnchessOnlineGameView(Context context){
        super(context);
//        this.setFocusable(true);
//        this.setFocusableInTouchMode(true);
        this.setZOrderOnTop(true);
        holder = this.getHolder();
        holder.addCallback(this);
        init();
    }
    private void init(){
        isMatchGame = ResponseHandler.getResHdler().getResObject().getResBody() instanceof MatchGameInfoRes;
        int disWidth = getResources().getDisplayMetrics().widthPixels;
        int disHeight = getResources().getDisplayMetrics().heightPixels;
        ratio = ((disHeight)>(1.0f*defaultHeight*disWidth/defaultWidth))?(1.0f*disWidth/defaultWidth):(1.0f*disHeight/defaultHeight);
        paintChessboard = new Paint();
        paintChessboard.setAntiAlias(true);
        paintChess = new Paint();
        paintChess.setAntiAlias(true);
        paintChess.setStrokeWidth(10*ratio);
        paintChess.setStyle(Paint.Style.STROKE);
        paintChessBackground = new Paint();
        paintChessBackground.setColor(Color.WHITE);
        paintStaticsBackgroud = new Paint();
        paintChessWrd = new Paint();
        paintChessWrd.setTextSize(50*ratio);
        Typeface font = Typeface.create(Typeface.SANS_SERIF, Typeface.BOLD);
        paintChessWrd.setTypeface( font );
        paintChessWrd.setAntiAlias(true);
        bmpChessboard = BitmapFactory.decodeResource(this.getResources(),R.drawable.chessboard);
        Matrix matrix = new Matrix();
        ratio2 = (defaultWidth > 1.0f * defaultHeight/bmpChessboard.getHeight()*bmpChessboard.getWidth())? 1.0f*defaultHeight/bmpChessboard.getHeight() : 1.0f*defaultWidth/bmpChessboard.getWidth();
        matrix.postScale(ratio*ratio2,ratio*ratio2);
        bmpChessboard = Bitmap.createBitmap(bmpChessboard,0,0,bmpChessboard.getWidth(),bmpChessboard.getHeight(),matrix,true);
        chessboard_sX = (int)(ratio*(defaultWidth-1080-2)/2);
        chessboard_sY = (int)(ratio*(defaultHeight-1435)/2);
        chessStatics_sX = (int)(ratio*120);
        chessStatics_sY_Enemy = (int)(ratio*165);
        chessStatics_sY = (int)(ratio * 1750);
        staticsHorizonInterval = (int)(ratio*35);
        staticsVerticalInterval = (int)(ratio*15);
        chessStaticsWidth = (int)(ratio*25);
        chess_sX = 42;chess_sY = 73;
        chessWidth = 155; chessHeight = 67;
        horizonInterval = 53; verticalInterval = 35; bigVerticalInterval = 135;
        action_sX = (int)(ratio * 40);action_sY_Enemy = (int)(ratio * 65);action_sY = (int)(ratio * 1650);
        chessboard = new Chess[12][5];
        gotoChessboard = new int[12][5];
        whoIsEnemy = 1;
        for(int i = 0;i<12;i++){ //******debug
            for(int j = 0;j<5;j++){
                chessboard[i][j] = new Chess(Chess.CHESS_NULL,Chess.STATE_ALIVE,false);
            }
        }
        userInfo_sX = (int)(ratio * 825);
        userInfo2_sX = (int)(ratio * 825);
        if(isMatchGame){
            canGo = ((MatchGameInfoRes)ResponseHandler.getResHdler().getResObject().getResBody()).isSelfStart();
            userInfo_sY = ((MatchGameInfoRes)ResponseHandler.getResHdler().getResObject().getResBody()).isSelfStart() ? (int)(ratio * 115) : (int)(ratio * 1700);
            userInfo2_sY = userInfo_sY + (int)(ratio * 50);
            opposeAccount = ((MatchGameInfoRes)ResponseHandler.getResHdler().getResObject().getResBody()).getAccount();
            opposeWinRate = ((MatchGameInfoRes)ResponseHandler.getResHdler().getResObject().getResBody()).getWinRate();
            chessboard = ((MatchGameInfoRes)ResponseHandler.getResHdler().getResObject().getResBody()).getChessBoard();
            opposeID = ((MatchGameInfoRes)ResponseHandler.getResHdler().getResObject().getResBody()).getUid();
        }else {
            canGo = ((FriendGameInfoRes)ResponseHandler.getResHdler().getResObject().getResBody()).isSelfStart();
            userInfo_sY = ((FriendGameInfoRes)ResponseHandler.getResHdler().getResObject().getResBody()).isSelfStart() ? (int)(ratio * 115) : (int)(ratio * 1700);
            userInfo2_sY = userInfo_sY + (int)(ratio * 50);
            opposeAccount = ((FriendGameInfoRes)ResponseHandler.getResHdler().getResObject().getResBody()).getAccount();
            opposeWinRate = ((FriendGameInfoRes)ResponseHandler.getResHdler().getResObject().getResBody()).getWinRate();
            chessboard = ((FriendGameInfoRes)ResponseHandler.getResHdler().getResObject().getResBody()).getChessBoard();
            opposeID = ((FriendGameInfoRes)ResponseHandler.getResHdler().getResObject().getResBody()).getUid();
        }
        state = -1;
        isDownBlue = true;
        chessStaticsUI = new int[2][13][3];  // for index of 1-dimension: 0:not discovered  1:alive 2: dead
        chessStatics = new int[2][13][3];  // for index of 1-dimension: 0:not discovered  1:alive 2: dead
        turnStatics = new int[2][2]; // for index of 1-dimension: 0:turn blue  1:turn red ;for index of 2-dimension: players
        ResponseHandler.getResHdler().setResponseListener(new ResponseHandler.ResponseListener() {
            @Override
            public void doevent(int status) {
                if(status == ResponseObject.RES_MATCHGAME){
                    MatchGameInfoRes matchGameInfoRes = (MatchGameInfoRes)ResponseHandler.getResHdler().getResObject().getResBody();
                    switch (matchGameInfoRes.getGameType()){
                        case MatchGameInfoRes.GAME_ON_TURN:
                            int i = matchGameInfoRes.getOldI();
                            int j = matchGameInfoRes.getOldJ();
                            if(i >= 0 && j >= 0 && i < 12 && j < 5) {   // for a undiscovered chess
                                if(chessboard[i][j].getArmyClass() != Chess.CHESS_NULL && chessboard[i][j].getState() == Chess.STATE_NOTDISCOVERED){
                                    for(int ii = 0; ii < 12; ii++){        //flush selected
                                        for(int jj = 0; jj < 5; jj++){
                                            if(chessboard[ii][jj].getState() == Chess.STATE_SELECT && (ii != i || jj != j)){
                                                chessboard[ii][jj].setState(Chess.STATE_ALIVE);
                                            }
                                            gotoChessboard[ii][jj] = 0;
                                        }
                                    }
                                    chessboard[i][j].setState(Chess.STATE_ALIVE);
                                    if(state == -1){                     //judge the side
                                        turnStatics[1-whoIsEnemy][chessboard[i][j].isEnemy() ? 1 : 0] += 1;
                                        for(int ii = 0; ii < 2;ii++){
                                            if(turnStatics[1-whoIsEnemy][ii] >= 2){
                                                isDownBlue = whoIsEnemy == 1 ? ii == 0: ii == 1;
                                                state = 0;
                                            }
                                        }
                                    }
                                    canGo = true;
                                    whoIsEnemy = 1 - whoIsEnemy;
                                }
                            }
                            break;
                        case MatchGameInfoRes.GAME_ON_MOVE:
                            int ii = matchGameInfoRes.getOldI();
                            int jj = matchGameInfoRes.getOldJ();
                            i = matchGameInfoRes.getNewI();
                            j = matchGameInfoRes.getNewJ();
                            if(chessboard[i][j].getArmyClass() == Chess.CHESS_NULL){   //move
                                Chess tmp = chessboard[i][j];
                                chessboard[i][j] = chessboard[ii][jj];
                                chessboard[ii][jj] = tmp;
                                canGo = true;
                            }else {    //eat
                                int flag = 0;
                                if(chessboard[i][j].getArmyClass() == Chess.CHESS_FLAG){
                                    flag = 1;
                                }

                                if(chessboard[ii][jj].getArmyClass() == chessboard[i][j].getArmyClass() || chessboard[i][j].getArmyClass() == Chess.CHESS_BOMB){
                                    chessboard[i][j] = new Chess(Chess.CHESS_NULL,Chess.STATE_ALIVE,true);
                                }else if(chessboard[ii][jj].getArmyClass() != Chess.CHESS_BOMB){
                                    chessboard[i][j] = chessboard[ii][jj];
                                }else {
                                    chessboard[i][j] = new Chess(Chess.CHESS_NULL,Chess.STATE_ALIVE,true);
                                }
                                chessboard[ii][jj] = new Chess(Chess.CHESS_NULL,Chess.STATE_ALIVE,false);

                                canGo = true;
                                if(flag == 1){
                                    _listener.onGameEnd(0);
                                }
                            }
                            state = 0;
                            whoIsEnemy = whoIsEnemy == 1 ? 0 : 1;
                            break;
                        default:
                            break;
                    }
                }else if(status == ResponseObject.RES_FRIENDGAME){
                    FriendGameInfoRes friendGameInfoRes = (FriendGameInfoRes)ResponseHandler.getResHdler().getResObject().getResBody();
                    switch (friendGameInfoRes.getGameType()){
                        case FriendGameInfoRes.GAME_ON_TURN:
                            int i = friendGameInfoRes.getOldI();
                            int j = friendGameInfoRes.getOldJ();
                            if(i >= 0 && j >= 0 && i < 12 && j < 5) {   // for a undiscovered chess
                                if(chessboard[i][j].getArmyClass() != Chess.CHESS_NULL && chessboard[i][j].getState() == Chess.STATE_NOTDISCOVERED){
                                    for(int ii = 0; ii < 12; ii++){        //flush selected
                                        for(int jj = 0; jj < 5; jj++){
                                            if(chessboard[ii][jj].getState() == Chess.STATE_SELECT && (ii != i || jj != j)){
                                                chessboard[ii][jj].setState(Chess.STATE_ALIVE);
                                            }
                                            gotoChessboard[ii][jj] = 0;
                                        }
                                    }
                                    chessboard[i][j].setState(Chess.STATE_ALIVE);
                                    if(state == -1){                     //judge the side
                                        turnStatics[1-whoIsEnemy][chessboard[i][j].isEnemy() ? 1 : 0] += 1;
                                        for(int ii = 0; ii < 2;ii++){
                                            if(turnStatics[1-whoIsEnemy][ii] >= 2){
                                                isDownBlue = whoIsEnemy == 1 ? ii == 0: ii == 1;
                                                state = 0;
                                            }
                                        }
                                    }
                                    canGo = true;
                                    whoIsEnemy = 1 - whoIsEnemy;
                                }
                            }
                            break;
                        case FriendGameInfoRes.GAME_ON_MOVE:
                            int ii = friendGameInfoRes.getOldI();
                            int jj = friendGameInfoRes.getOldJ();
                            i = friendGameInfoRes.getNewI();
                            j = friendGameInfoRes.getNewJ();
                            if(chessboard[i][j].getArmyClass() == Chess.CHESS_NULL){   //move
                                Chess tmp = chessboard[i][j];
                                chessboard[i][j] = chessboard[ii][jj];
                                chessboard[ii][jj] = tmp;
                                canGo = true;
                            }else {    //eat
                                int flag = 0;
                                if(chessboard[i][j].getArmyClass() == Chess.CHESS_FLAG){
                                    flag = 1;
                                }

                                if(chessboard[ii][jj].getArmyClass() == chessboard[i][j].getArmyClass() || chessboard[i][j].getArmyClass() == Chess.CHESS_BOMB){
                                    chessboard[i][j] = new Chess(Chess.CHESS_NULL,Chess.STATE_ALIVE,true);
                                }else if(chessboard[ii][jj].getArmyClass() != Chess.CHESS_BOMB){
                                    chessboard[i][j] = chessboard[ii][jj];
                                }else {
                                    chessboard[i][j] = new Chess(Chess.CHESS_NULL,Chess.STATE_ALIVE,true);
                                }
                                chessboard[ii][jj] = new Chess(Chess.CHESS_NULL,Chess.STATE_ALIVE,false);

                                canGo = true;
                                if(flag == 1){
                                    _listener.onGameEnd(0);
                                }
                            }
                            state = 0;
                            whoIsEnemy = whoIsEnemy == 1 ? 0 : 1;
                            break;
                        default:
                            break;
                    }
                }
            }
        });
        if(!canGo){
            new Thread(new Runnable() {
                @Override
                public void run() {
                    ResponseHandler.getResHdler().readMessage();
                }
            }).start();
        }
    }
    @Override
    public void surfaceChanged(SurfaceHolder holder,int format, int width, int height){
    }
    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        renderThread = new RenderThread();
        isDraw = true;
        renderThread.start();
    }
    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        isDraw = false;
    }
    private class RenderThread extends Thread{
        @Override
        public void run(){
            while (isDraw) {
                drawUI();
            }
        }
    }
    private void drawUI(){
        Canvas canvas = holder.lockCanvas();
        try {
            if(canvas != null){
                synchronized (holder) {
                    drawCanvas(canvas);
                }
            }
            Thread.sleep(30);
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            if(canvas != null){
                holder.unlockCanvasAndPost(canvas);
            }
        }
    }
    private void drawCanvas(Canvas canvas){
        canvas.drawColor(Color.WHITE);
        canvas.drawBitmap(bmpChessboard,chessboard_sX,chessboard_sY,paintChessboard);
        paintChessWrd.setTextSize(50*ratio);
        paintChess.setStrokeWidth(10*ratio);
        for(int i = 0; i < 12; i++){
            for(int j = 0; j < 5; j++){
                if(chessboard[i][j] != null){
                    if(chessboard[i][j].getArmyClass() != Chess.CHESS_NULL){
                        int x = mapX(j);
                        int y = mapY(i);
                        if(chessboard[i][j].getState() == Chess.STATE_NOTDISCOVERED){
                            paintChessBackground.setColor(Color.GRAY);
                            canvas.drawRect(x,y,x+ratio*chessWidth,y+ratio*chessHeight,paintChessBackground);
                        }else {
                            paintChessBackground.setColor(Color.WHITE);
                            paintChess.setColor((chessboard[i][j].isEnemy())?(chessboard[i][j].getState() == Chess.STATE_SELECT ? Color.YELLOW:Color.RED):(chessboard[i][j].getState() == Chess.STATE_SELECT ? Color.CYAN:Color.BLUE));
                            canvas.drawRect(x,y,x+ratio*chessWidth,y+ratio*chessHeight,paintChessBackground);
                            canvas.drawRect(x,y,x+ratio*chessWidth,y+ratio*chessHeight,paintChess);
                            paintChessWrd.setColor(chessboard[i][j].isEnemy()?Color.RED:Color.BLUE);
                            String s = null;
                            switch (chessboard[i][j].getArmyClass()){
                                case Chess.CHESS_COMMANDER:    s="司令";break;
                                case Chess.CHESS_ARMY:          s="军长";break;
                                case Chess.CHESS_TEACHER:       s="师长";break;
                                case Chess.CHESS_BRIGADIER:     s="旅长";break;
                                case Chess.CHESS_COLONEL:       s="团长";break;
                                case Chess.CHESS_BATTALION:     s="营长";break;
                                case Chess.CHESS_COMPANY:       s="连长";break;
                                case Chess.CHESS_PLATOON:       s="排长";break;
                                case Chess.CHESS_SAPPER:        s="工兵";break;
                                case Chess.CHESS_MINE:          s="地雷";break;
                                case Chess.CHESS_BOMB:          s="炸弹";break;
                                case Chess.CHESS_FLAG:          s="军旗";break;
                                default:break;
                            }
                            canvas.drawText(s, x+ ratio*chessWidth/6 ,y + ratio*3*chessHeight/4,paintChessWrd);
                        }
                        chessStaticsUI[chessboard[i][j].isEnemy() ? 1:0][chessboard[i][j].getArmyClass()][chessboard[i][j].getState() == Chess.STATE_SELECT ? Chess.STATE_ALIVE:chessboard[i][j].getState()] += 1;
                    }
                }
                if(gotoChessboard[i][j] == 1){
                    int x = mapX(j);
                    int y = mapY(i);
                    paintChess.setColor(Color.GREEN);
                    canvas.drawRect(x,y,x+ratio*chessWidth,y+ratio*chessHeight,paintChess);
                }
            }
        }
        paintChessWrd.setTextSize(50*ratio);
        paintChessWrd.setColor(state == -1 ? Color.GRAY : (whoIsEnemy == 0 ? (isDownBlue ? Color.RED : Color.BLUE) : (isDownBlue ? Color.BLUE : Color.RED)));
        canvas.drawText(canGo?"我的回合":"敌方回合",action_sX,(whoIsEnemy == 0 ?action_sY_Enemy: action_sY) + 10*ratio,paintChessWrd);

        paintChessWrd.setTextSize(30*ratio);
        paintChess.setColor(Color.BLACK);
        paintChess.setStrokeWidth(5*ratio);
        for(int enemy = 1; enemy >=0; enemy--){
            paintChessWrd.setColor(enemy == 1 ? (isDownBlue ? Color.RED : Color.BLUE) : (isDownBlue ? Color.BLUE : Color.RED));
            for(int i = Chess.CHESS_COMMANDER; i <= Chess.CHESS_FLAG;i++){
                int k = 0;
                String s = null;
                switch (i){
                    case Chess.CHESS_COMMANDER:    s="司";k=1;break;
                    case Chess.CHESS_ARMY:          s="军";k=1;break;
                    case Chess.CHESS_TEACHER:       s="师";k=2;break;
                    case Chess.CHESS_BRIGADIER:     s="旅";k=2;break;
                    case Chess.CHESS_COLONEL:       s="团";k=2;break;
                    case Chess.CHESS_BATTALION:     s="营";k=2;break;
                    case Chess.CHESS_COMPANY:       s="连";k=3;break;
                    case Chess.CHESS_PLATOON:       s="排";k=3;break;
                    case Chess.CHESS_SAPPER:        s="工";k=3;break;
                    case Chess.CHESS_MINE:          s="雷";k=3;break;
                    case Chess.CHESS_BOMB:          s="炸";k=2;break;
                    case Chess.CHESS_FLAG:          s="旗";k=1;break;
                    default:break;
                }
                canvas.drawText(s,chessStatics_sX+(i-1)*(chessStaticsWidth+staticsHorizonInterval),(enemy == 1?chessStatics_sY_Enemy: chessStatics_sY) + 10*ratio,paintChessWrd);
                for(int j = 0; j < k;j++){
                    paintStaticsBackgroud.setColor(chessStaticsUI[isDownBlue?enemy:1-enemy][i][0] >= (j+1) ? Color.GRAY:(chessStaticsUI[isDownBlue?enemy:1-enemy][i][1] >= (j+1-chessStaticsUI[isDownBlue?enemy:1-enemy][i][0]) ? (enemy == 1? (isDownBlue ? Color.RED: Color.BLUE):(isDownBlue ? Color.BLUE: Color.RED)):Color.WHITE));
                    canvas.drawRect(chessStatics_sX+(i-1)*(chessStaticsWidth+staticsHorizonInterval),(enemy == 1?chessStatics_sY_Enemy: chessStatics_sY)-2*chessStaticsWidth-j*(chessStaticsWidth+staticsVerticalInterval),chessStatics_sX+chessStaticsWidth+(i-1)*(chessStaticsWidth+staticsHorizonInterval),(enemy == 1?chessStatics_sY_Enemy: chessStatics_sY)-chessStaticsWidth-j*(chessStaticsWidth+staticsVerticalInterval),paintStaticsBackgroud);
                    canvas.drawRect(chessStatics_sX+(i-1)*(chessStaticsWidth+staticsHorizonInterval),(enemy == 1?chessStatics_sY_Enemy: chessStatics_sY)-2*chessStaticsWidth-j*(chessStaticsWidth+staticsVerticalInterval),chessStatics_sX+chessStaticsWidth+(i-1)*(chessStaticsWidth+staticsHorizonInterval),(enemy == 1?chessStatics_sY_Enemy: chessStatics_sY)-chessStaticsWidth-j*(chessStaticsWidth+staticsVerticalInterval),paintChess);
                }
            }
        }
        paintChessWrd.setTextSize(35*ratio);
        paintChessWrd.setColor(Color.RED);
        canvas.drawText("对手昵称: "+opposeAccount,userInfo_sX,userInfo_sY,paintChessWrd);
        canvas.drawText("胜率: "+opposeWinRate,userInfo_sX,userInfo2_sY,paintChessWrd);
        for(int i = 0; i <= 1;i++){
            for(int j = 0; j <=12 ; j++){
                for(int k = 0; k <= 2; k++){
                    if(chessStatics[i][j][k] != chessStaticsUI[i][j][k]){
                        for(int ii = 0; ii <= 1;ii++){
                            for(int jj = 0; jj <=12 ; jj++){
                                for(int kk = 0; kk <= 2; kk++){
                                    chessStatics[ii][jj][kk] = chessStaticsUI[ii][jj][kk];
                                    chessStaticsUI[ii][jj][kk] = 0;
                                }
                            }
                        }
                        return;
                    }
                }
            }
        }

        //System.out.println(chessStaticsUI[1][Chess.CHESS_ARMY][2]);
        for(int i = 0; i <= 1;i++){
            for(int j = 0; j <=12 ; j++){
                for(int k = 0; k <= 2; k++){
                    chessStaticsUI[i][j][k] = 0;
                }
            }
        }
    }

    public void setDraw(boolean draw){
        isDraw = draw;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event){
        if(event.getAction() == MotionEvent.ACTION_UP || event.getAction() == MotionEvent.ACTION_MOVE){
            return true;
        }
        //System.out.println("("+event.getX()+","+event.getY()+")");
        int i = mapI((int)event.getY());
        int j = mapJ((int)event.getX());

        if(canGo){
            if(i >= 0 && j >= 0 && i < 12 && j < 5) {   // for a undiscovered chess
                if(chessboard[i][j].getArmyClass() != Chess.CHESS_NULL && chessboard[i][j].getState() == Chess.STATE_NOTDISCOVERED){
                    for(int ii = 0; ii < 12; ii++){        //flush selected
                        for(int jj = 0; jj < 5; jj++){
                            if(chessboard[ii][jj].getState() == Chess.STATE_SELECT && (ii != i || jj != j)){
                                chessboard[ii][jj].setState(Chess.STATE_ALIVE);
                            }
                            gotoChessboard[ii][jj] = 0;
                        }
                    }
                    chessboard[i][j].setState(Chess.STATE_ALIVE);
                    if(state == -1){                     //judge the side
                        turnStatics[1-whoIsEnemy][chessboard[i][j].isEnemy() ? 1 : 0] += 1;
                        for(int ii = 0; ii < 2;ii++){
                            if(turnStatics[1-whoIsEnemy][ii] >= 2){
                                isDownBlue = whoIsEnemy == 1 ? ii == 0: ii == 1;
                                state = 0;
                            }
                        }
                    }
                    oldI = i;
                    oldJ = j;
                    canGo = false;
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            if(isMatchGame){
                                MatchGameInfo matchGameInfo = new MatchGameInfo(MatchGameInfo.GAME_ON_TURN);
                                matchGameInfo.setOldI(oldI);
                                matchGameInfo.setOldJ(oldJ);
                                matchGameInfo.setUid(opposeID);
                                RequestHandler.getReqHdler().sendMessage(new RequestObject(RequestObject.REQ_MATCHGAME,matchGameInfo));
                                ResponseHandler.getResHdler().readMessage();
                            }else {
                                FriendGameInfo friendGameInfo = new FriendGameInfo(FriendGameInfo.GAME_ON_TURN);
                                friendGameInfo.setOldI(oldI);
                                friendGameInfo.setOldJ(oldJ);
                                friendGameInfo.setUid(opposeID);
                                RequestHandler.getReqHdler().sendMessage(new RequestObject(RequestObject.REQ_FRIENDGAME,friendGameInfo));
                                ResponseHandler.getResHdler().readMessage();
                            }
                        }
                    }).start();
                    whoIsEnemy = 1 - whoIsEnemy;
                    this.invalidate();
                    return true;
                }
            }
            if(state == 1){              // for a discovered chess
                if(i >= 0 && j >= 0 && i < 12 && j < 5) {     //valid select
                    if (gotoChessboard[i][j] != 0){               //move or eat
                        if(chessboard[i][j].getArmyClass() == Chess.CHESS_NULL){   //move
                            Chess tmp = chessboard[i][j];
                            for(int ii = 0; ii < 12; ii++){        //find selected
                                for(int jj = 0; jj < 5; jj++){
                                    if(chessboard[ii][jj].getState() == Chess.STATE_SELECT && (ii != i || jj != j)){
                                        chessboard[i][j] = chessboard[ii][jj];
                                        chessboard[ii][jj] = tmp;
                                    }
                                }
                            }
                            newI = i;
                            newJ = j;
                            canGo = false;
                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    if(isMatchGame){
                                        MatchGameInfo matchGameInfo = new MatchGameInfo(MatchGameInfo.GAME_ON_MOVE);
                                        matchGameInfo.setOldI(oldI);
                                        matchGameInfo.setOldJ(oldJ);
                                        matchGameInfo.setNewI(newI);
                                        matchGameInfo.setNewJ(newJ);
                                        matchGameInfo.setUid(opposeID);
                                        RequestHandler.getReqHdler().sendMessage(new RequestObject(RequestObject.REQ_MATCHGAME,matchGameInfo));
                                        ResponseHandler.getResHdler().readMessage();
                                    }else {
                                        FriendGameInfo friendGameInfo = new FriendGameInfo(FriendGameInfo.GAME_ON_MOVE);
                                        friendGameInfo.setOldI(oldI);
                                        friendGameInfo.setOldJ(oldJ);
                                        friendGameInfo.setNewI(newI);
                                        friendGameInfo.setNewJ(newJ);
                                        friendGameInfo.setUid(opposeID);
                                        RequestHandler.getReqHdler().sendMessage(new RequestObject(RequestObject.REQ_FRIENDGAME,friendGameInfo));
                                        ResponseHandler.getResHdler().readMessage();
                                    }
                                }
                            }).start();
                        }else {    //eat
                            int flag = 0;
                            if(chessboard[i][j].getArmyClass() == Chess.CHESS_FLAG){
                                flag = 1;
                            }
                            for(int ii = 0; ii < 12; ii++){        //find selected
                                for(int jj = 0; jj < 5; jj++){
                                    if(chessboard[ii][jj].getState() == Chess.STATE_SELECT && (ii != i || jj != j)){
                                        if(chessboard[ii][jj].getArmyClass() == chessboard[i][j].getArmyClass() || chessboard[i][j].getArmyClass() == Chess.CHESS_BOMB){
                                            chessboard[i][j] = new Chess(Chess.CHESS_NULL,Chess.STATE_ALIVE,true);
                                        }else if(chessboard[ii][jj].getArmyClass() != Chess.CHESS_BOMB){
                                            chessboard[i][j] = chessboard[ii][jj];
                                        }else {
                                            chessboard[i][j] = new Chess(Chess.CHESS_NULL,Chess.STATE_ALIVE,true);
                                        }
                                        chessboard[ii][jj] = new Chess(Chess.CHESS_NULL,Chess.STATE_ALIVE,false);
                                    }
                                }
                            }
                            newI = i;
                            newJ = j;
                            canGo = false;
                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    if(isMatchGame){
                                        MatchGameInfo matchGameInfo = new MatchGameInfo(MatchGameInfo.GAME_ON_MOVE);
                                        matchGameInfo.setOldI(oldI);
                                        matchGameInfo.setOldJ(oldJ);
                                        matchGameInfo.setNewI(newI);
                                        matchGameInfo.setNewJ(newJ);
                                        matchGameInfo.setUid(opposeID);
                                        RequestHandler.getReqHdler().sendMessage(new RequestObject(RequestObject.REQ_MATCHGAME,matchGameInfo));
                                    }else {
                                        FriendGameInfo friendGameInfo = new FriendGameInfo(FriendGameInfo.GAME_ON_MOVE);
                                        friendGameInfo.setOldI(oldI);
                                        friendGameInfo.setOldJ(oldJ);
                                        friendGameInfo.setNewI(newI);
                                        friendGameInfo.setNewJ(newJ);
                                        friendGameInfo.setUid(opposeID);
                                        RequestHandler.getReqHdler().sendMessage(new RequestObject(RequestObject.REQ_FRIENDGAME,friendGameInfo));
                                    }
                                }
                            }).start();
                            if(flag == 1){
                                _listener.onGameEnd(1);
                            }else {
                                new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        ResponseHandler.getResHdler().readMessage();
                                    }
                                }).start();
                            }
                        }
                        state = 0;
                        whoIsEnemy = whoIsEnemy == 1 ? 0 : 1;
                        for(int ii = 0; ii < 12; ii++){        //flush selected
                            for(int jj = 0; jj < 5; jj++){
                                if(chessboard[ii][jj].getState() == Chess.STATE_SELECT){
                                    chessboard[ii][jj].setState(Chess.STATE_ALIVE);
                                }
                                gotoChessboard[ii][jj] = 0;
                            }
                        }
                    }else {                       //select other chess
                        state = 0;
                        for(int ii = 0; ii < 12; ii++){        //flush selected
                            for(int jj = 0; jj < 5; jj++){
                                if(chessboard[ii][jj].getState() == Chess.STATE_SELECT && (ii != i || jj != j)){
                                    chessboard[ii][jj].setState(Chess.STATE_ALIVE);
                                }
                                gotoChessboard[ii][jj] = 0;
                            }
                        }
                        if(i >= 0 && j >= 0 && i < 12 && j < 5){                //select a chess
                            if(chessboard[i][j].getState() != Chess.STATE_DEAD && chessboard[i][j].getArmyClass() != Chess.CHESS_NULL && chessboard[i][j].isEnemy()!=(isDownBlue?whoIsEnemy == 1:whoIsEnemy == 0)){
                                chessboard[i][j].setState(Chess.STATE_SELECT);
                                gotoChessboard = showPath(chessboard,i,j);
                                state = 1;
                                oldI = i;
                                oldJ = j;
                            }
                        }
                    }
                }else {               //invalid select
                    state = 0;
                    for(int ii = 0; ii < 12; ii++){        //flush selected
                        for(int jj = 0; jj < 5; jj++){
                            if(chessboard[ii][jj].getState() == Chess.STATE_SELECT && (ii != i || jj != j)){
                                chessboard[ii][jj].setState(Chess.STATE_ALIVE);
                            }
                            gotoChessboard[ii][jj] = 0;
                        }
                    }
                }
            }else if (state == 0){
                for(int ii = 0; ii < 12; ii++){        //flush selected
                    for(int jj = 0; jj < 5; jj++){
                        if(chessboard[ii][jj].getState() == Chess.STATE_SELECT && (ii != i || jj != j)){
                            chessboard[ii][jj].setState(Chess.STATE_ALIVE);
                        }
                        gotoChessboard[ii][jj] = 0;
                    }
                }
                if(i >= 0 && j >= 0 && i < 12 && j < 5){                //select a chess
                    if(chessboard[i][j].getState() != Chess.STATE_DEAD && chessboard[i][j].getArmyClass() != Chess.CHESS_NULL && chessboard[i][j].isEnemy()!=(isDownBlue?whoIsEnemy == 1:whoIsEnemy == 0)){
                        chessboard[i][j].setState(Chess.STATE_SELECT);
                        gotoChessboard = showPath(chessboard,i,j);
                        state = 1;
                        oldI = i;
                        oldJ = j;
                    }
                }
            }
        }
        this.invalidate();
        return true;
    }
    public int mapY(int i){
        if(i > 5){
            return (int)(chessboard_sY + ratio * (chess_sY + (i-1) * verticalInterval + bigVerticalInterval + i * chessHeight));
        }else{
            return (int)(chessboard_sY + ratio * (chess_sY + i * (verticalInterval+chessHeight)));
        }
    }
    public int mapX(int j){
        return (int)(chessboard_sX + ratio * (chess_sX + j * (horizonInterval+chessWidth)));
    }
    public int mapI(int y){
        int h = 0;
        if(y < chessboard_sY + ratio * chess_sY || (y > mapY(5)+chessHeight && y < mapY(6))){
            return -1;
        }
        if(y > chessboard_sY + bmpChessboard.getHeight()/2){
            h = (int)(((y - chessboard_sY)/ratio -chess_sY - bigVerticalInterval + verticalInterval)/(verticalInterval + chessHeight));
        }else {
            h = (int)(((y - chessboard_sY)/ratio -chess_sY)/(verticalInterval + chessHeight));
        }
        return y - mapY(h) > chessHeight ? -1 : h;
    }
    public int mapJ(int x){
        if(x <chessboard_sX + ratio * chess_sX){
            return -1;
        }
        int w = (int)(((x - chessboard_sX)/ratio - chess_sX)/(horizonInterval+chessWidth));
        return x - mapX(w) > chessWidth ? -1 : w;
    }
    private int[][] showPath(Chess[][] x, int i, int j){
        int[][] available = new int[12][5];
        if(x[i][j].getArmyClass() == Chess.CHESS_MINE || x[i][j].getArmyClass() == Chess.CHESS_FLAG){
            return available;
        }else if(x[i][j].getArmyClass() == Chess.CHESS_SAPPER && ((i == 1 || i == 5 || i == 6 || i == 10 )||(j == 0 || j == 2 || j == 4 && i != 0 && i != 11))){
            specSapperPath(available,x,i,j,x[i][j].isEnemy());
            available[i][j] = 0;
        }
        if(i == 1 || i == 10){           // chess position situation: 2rd row or 11th row  or  6th row or 7th row
            if(j == 1 || j == 3){           // chess at 2 or 4 column
                if(x[i+1][j].getArmyClass() == Chess.CHESS_NULL || (eat(x[i][j].getArmyClass(),x[i+1][j].getArmyClass()) && x[i][j].isEnemy()!= x[i+1][j].isEnemy() && x[i+1][j].getState() != Chess.STATE_NOTDISCOVERED && (i + 1)!=2 )){          //no chess or chess can be eaten
                    available[i+1][j] = 1;
                }
                if(x[i-1][j].getArmyClass() == Chess.CHESS_NULL || (eat(x[i][j].getArmyClass(),x[i-1][j].getArmyClass()) && x[i][j].isEnemy()!= x[i-1][j].isEnemy() && x[i-1][j].getState() != Chess.STATE_NOTDISCOVERED && (i - 1)!=9 )){          //no chess or chess can be eaten
                    available[i-1][j] = 1;
                }
            }else if (j == 2){
                if(x[i+1][j].getArmyClass() == Chess.CHESS_NULL || (eat(x[i][j].getArmyClass(),x[i+1][j].getArmyClass()) && x[i][j].isEnemy()!= x[i+1][j].isEnemy() && x[i+1][j].getState() != Chess.STATE_NOTDISCOVERED)){          //no chess or chess can be eaten
                    available[i+1][j] = 1;
                }
                if(x[i-1][j].getArmyClass() == Chess.CHESS_NULL || (eat(x[i][j].getArmyClass(),x[i-1][j].getArmyClass()) && x[i][j].isEnemy()!= x[i-1][j].isEnemy() && x[i-1][j].getState() != Chess.STATE_NOTDISCOVERED)){          //no chess or chess can be eaten
                    available[i-1][j] = 1;
                }
                if(x[i == 1 ? i+1:i-1][j+1].getArmyClass() == Chess.CHESS_NULL ){          //no chess
                    available[i == 1 ? i+1:i-1][j+1] = 1;
                }
                if(x[i == 1 ? i+1:i-1][j-1].getArmyClass() == Chess.CHESS_NULL ){          //no chess
                    available[i == 1 ? i+1:i-1][j-1] = 1;
                }
            }else if(j == 0 || j == 4){
                int ii = i + 1;
                while(ii < 11 && x[ii][j].getArmyClass() == Chess.CHESS_NULL ){
                    available[ii][j] = 1;
                    ii++;
                }
                if(ii < 11 && eat(x[i][j].getArmyClass(),x[ii][j].getArmyClass()) && x[i][j].isEnemy()!= x[ii][j].isEnemy() && x[ii][j].getState() != Chess.STATE_NOTDISCOVERED){
                    available[ii][j] = 1;
                }
                ii = i - 1;
                while(ii > 0 && x[ii][j].getArmyClass() == Chess.CHESS_NULL ){
                    available[ii][j] = 1;
                    ii--;
                }
                if(ii > 0 && eat(x[i][j].getArmyClass(),x[ii][j].getArmyClass()) && x[i][j].isEnemy()!= x[ii][j].isEnemy() && x[ii][j].getState() != Chess.STATE_NOTDISCOVERED){
                    available[ii][j] = 1;
                }
                if(x[i == 1? i+1 : i-1][j == 0? j+1:j-1].getArmyClass() == Chess.CHESS_NULL ){          //no chess
                    available[i == 1? i+1 : i-1][j == 0? j+1:j-1] = 1;
                }
                if(x[i == 1? i-1 : i+1][j].getArmyClass() == Chess.CHESS_NULL || (eat(x[i][j].getArmyClass(),x[i == 1? i-1 : i+1][j].getArmyClass()) && x[i][j].isEnemy()!= x[i == 1? i-1 : i+1][j].isEnemy() && x[i == 1? i-1 : i+1][j].getState() != Chess.STATE_NOTDISCOVERED)){          //no chess or chess can be eaten
                    available[i == 1? i-1 : i+1][j] = 1;
                }
            }
            int jj = j + 1;
            while(jj < 5 && x[i][jj].getArmyClass() == Chess.CHESS_NULL ){
                available[i][jj] = 1;
                jj++;
            }
            if(jj < 5 && eat(x[i][j].getArmyClass(),x[i][jj].getArmyClass()) && x[i][j].isEnemy()!= x[i][jj].isEnemy() && x[i][jj].getState() != Chess.STATE_NOTDISCOVERED){
                available[i][jj] = 1;
            }
            jj = j - 1;
            while(jj>= 0 && x[i][jj].getArmyClass() == Chess.CHESS_NULL ){
                available[i][jj] = 1;
                jj--;
            }
            if(jj >= 0 && eat(x[i][j].getArmyClass(),x[i][jj].getArmyClass()) && x[i][j].isEnemy()!= x[i][jj].isEnemy() && x[i][jj].getState() != Chess.STATE_NOTDISCOVERED){
                available[i][jj] = 1;
            }
        }else if(i == 2 || i == 9 || i == 4 || i == 7) {           // chess position situation: 3rd row or 10th row  or  5rd row or 8th row
            if (j == 1 || j == 3) {
                for (int ii = i - 1; ii <= i + 1; ii++) {
                    for (int jj = j - 1; jj <= j + 1; jj++) {
                        if (ii == i && jj == j) {
                            continue;
                        }
                        if (x[ii][jj].getArmyClass() == Chess.CHESS_NULL || (eat(x[i][j].getArmyClass(), x[ii][jj].getArmyClass()) && x[i][j].isEnemy()!= x[ii][jj].isEnemy() && x[ii][jj].getState() != Chess.STATE_NOTDISCOVERED)) {          //no chess or chess can be eaten
                            available[ii][jj] = 1;
                        }
                    }
                }
                available[(i == 2 || i == 7) ? i + 1 : i - 1][j == 1 ? j + 1 : j - 1] = 0;
                if (x[(i == 2 || i == 7) ? i + 1 : i - 1][j == 1 ? j + 1 : j - 1].getArmyClass() == Chess.CHESS_NULL) {          //no chess or chess can be eaten
                    available[(i == 2 || i == 7) ? i + 1 : i - 1][j == 1 ? j + 1 : j - 1] = 1;
                }
            }else if(j == 2){
                if(x[(i==2||i==7)?i-1:i+1][j].getArmyClass() == Chess.CHESS_NULL || (eat(x[i][j].getArmyClass(), x[(i==2||i==7)?i-1:i+1][j].getArmyClass()) && x[i][j].isEnemy()!= x[(i==2||i==7)?i-1:i+1][j].isEnemy() && x[(i==2||i==7)?i-1:i+1][j].getState() != Chess.STATE_NOTDISCOVERED)){          //no chess or chess can be eaten
                    available[(i==2||i==7)?i-1:i+1][j] = 1;
                }
                if(x[(i==2||i==7)?i+1:i-1][j].getArmyClass() == Chess.CHESS_NULL ){          //no chess
                    available[(i==2||i==7)?i+1:i-1][j] = 1;
                }
                if(x[i][j+1].getArmyClass() == Chess.CHESS_NULL ){          //no chess
                    available[i][j+1] = 1;
                }
                if(x[i][j-1].getArmyClass() == Chess.CHESS_NULL ){          //no chess
                    available[i][j-1] = 1;
                }
            }else if(j == 0 || j == 4 ){
                if(x[i][j == 0? j+1:j-1].getArmyClass() == Chess.CHESS_NULL ){          //no chess
                    available[i][j == 0? j+1:j-1] = 1;
                }
                int ii = i + 1;
                while(ii < 11 && x[ii][j].getArmyClass() == Chess.CHESS_NULL ){
                    available[ii][j] = 1;
                    ii++;
                }
                if(ii < 11 && eat(x[i][j].getArmyClass(),x[ii][j].getArmyClass()) && x[i][j].isEnemy()!= x[ii][j].isEnemy() && x[ii][j].getState() != Chess.STATE_NOTDISCOVERED){
                    available[ii][j] = 1;
                }
                ii = i - 1;
                while(ii > 0 && x[ii][j].getArmyClass() == Chess.CHESS_NULL ){
                    available[ii][j] = 1;
                    ii--;
                }
                if(ii > 0 && eat(x[i][j].getArmyClass(),x[ii][j].getArmyClass()) && x[i][j].isEnemy()!= x[ii][j].isEnemy() && x[ii][j].getState() != Chess.STATE_NOTDISCOVERED){
                    available[ii][j] = 1;
                }
            }
        }else  if(i == 3 || i == 8) {           // chess position situation: 4rd row or 9th row
            if(j == 1 || j == 3){
                if(x[i][j==1?j-1:j+1].getArmyClass() == Chess.CHESS_NULL || (eat(x[i][j].getArmyClass(), x[i][j==1?j-1:j+1].getArmyClass()) && x[i][j].isEnemy()!= x[i][j==1?j-1:j+1].isEnemy() && x[i][j==1?j-1:j+1].getState() != Chess.STATE_NOTDISCOVERED)){          //no chess or chess can be eaten
                    available[i][j==1?j-1:j+1] = 1;
                }
                if(x[i][j==1?j+1:j-1].getArmyClass() == Chess.CHESS_NULL ){          //no chess
                    available[i][j==1?j+1:j-1] = 1;
                }
                if(x[i+1][j].getArmyClass() == Chess.CHESS_NULL ){          //no chess
                    available[i+1][j] = 1;
                }
                if(x[i-1][j].getArmyClass() == Chess.CHESS_NULL ){          //no chess
                    available[i-1][j] = 1;
                }
            }else if(j == 2){
                for (int ii = i - 1; ii <= i + 1; ii++) {
                    for (int jj = j - 1; jj <= j + 1; jj++) {
                        if (ii == i && jj == j) {
                            continue;
                        }
                        if(ii == i || jj == j){
                            if (x[ii][jj].getArmyClass() == Chess.CHESS_NULL || (eat(x[i][j].getArmyClass(), x[ii][jj].getArmyClass()) && x[i][j].isEnemy()!= x[ii][jj].isEnemy() && x[ii][jj].getState() != Chess.STATE_NOTDISCOVERED)) {          //no chess or chess can be eaten
                                available[ii][jj] = 1;
                            }
                        }else {
                            if (x[ii][jj].getArmyClass() == Chess.CHESS_NULL ) {          //no chess
                                available[ii][jj] = 1;
                            }
                        }
                    }
                }
            }else if(j == 0 || j == 4){
                if (x[i==3?i+1:i-1][j==0?j+1:j-1].getArmyClass() == Chess.CHESS_NULL ) {          //no chess
                    available[i==3?i+1:i-1][j==0?j+1:j-1] = 1;
                }
                if (x[i==3?i-1:i+1][j==0?j+1:j-1].getArmyClass() == Chess.CHESS_NULL ) {          //no chess
                    available[i==3?i-1:i+1][j==0?j+1:j-1] = 1;
                }
                if(x[i][j==0?j+1:j-1].getArmyClass() == Chess.CHESS_NULL || (eat(x[i][j].getArmyClass(), x[i][j==0?j+1:j-1].getArmyClass()) && x[i][j].isEnemy()!= x[i][j==0?j+1:j-1].isEnemy() && x[i][j==0?j+1:j-1].getState() != Chess.STATE_NOTDISCOVERED)){          //no chess or chess can be eaten
                    available[i][j==0?j+1:j-1] = 1;
                }
                int ii = i + 1;
                while(ii < 11 && x[ii][j].getArmyClass() == Chess.CHESS_NULL ){
                    available[ii][j] = 1;
                    ii++;
                }
                if(ii < 11 && eat(x[i][j].getArmyClass(),x[ii][j].getArmyClass()) && x[i][j].isEnemy()!= x[ii][j].isEnemy() && x[ii][j].getState() != Chess.STATE_NOTDISCOVERED){
                    available[ii][j] = 1;
                }
                ii = i - 1;
                while(ii > 0 && x[ii][j].getArmyClass() == Chess.CHESS_NULL ){
                    available[ii][j] = 1;
                    ii--;
                }
                if(ii > 0 && eat(x[i][j].getArmyClass(),x[ii][j].getArmyClass()) && x[i][j].isEnemy()!= x[ii][j].isEnemy() && x[ii][j].getState() != Chess.STATE_NOTDISCOVERED){
                    available[ii][j] = 1;
                }
            }
        }else if(i == 5 || i == 6){      // chess position situation: 6rd row or 7th row
            if(j == 1 || j == 3){
                if(x[i==5?i-1:i+1][j].getArmyClass() == Chess.CHESS_NULL ){          //no chess
                    available[i==5?i-1:i+1][j] = 1;
                }
            }else if(j == 2){
                if(x[i+1][j].getArmyClass() == Chess.CHESS_NULL || (eat(x[i][j].getArmyClass(), x[i+1][j].getArmyClass()) && x[i][j].isEnemy()!= x[i+1][j].isEnemy() && x[i+1][j].getState() != Chess.STATE_NOTDISCOVERED)){          //no chess or chess can be eaten
                    available[i+1][j] = 1;
                }
                if(x[i-1][j].getArmyClass() == Chess.CHESS_NULL || (eat(x[i][j].getArmyClass(), x[i-1][j].getArmyClass()) && x[i][j].isEnemy()!= x[i-1][j].isEnemy()&& x[i-1][j].getState() != Chess.STATE_NOTDISCOVERED)){          //no chess or chess can be eaten
                    available[i-1][j] = 1;
                }
                if(x[i==5?i-1:i+1][j-1].getArmyClass() == Chess.CHESS_NULL){
                    available[i==5?i-1:i+1][j-1] = 1;
                }
                if(x[i==5?i-1:i+1][j+1].getArmyClass() == Chess.CHESS_NULL){
                    available[i==5?i-1:i+1][j+1] = 1;
                }
            }else if(j == 0 || j == 4){
                if(x[i==5?i-1:i+1][j==0?j+1:j-1].getArmyClass() == Chess.CHESS_NULL){
                    available[i==5?i-1:i+1][j==0?j+1:j-1] = 1;
                }
                int ii = i + 1;
                while(ii < 11 && x[ii][j].getArmyClass() == Chess.CHESS_NULL ){
                    available[ii][j] = 1;
                    ii++;
                }
                if(ii < 11 && eat(x[i][j].getArmyClass(),x[ii][j].getArmyClass()) && x[i][j].isEnemy()!= x[ii][j].isEnemy() && x[ii][j].getState() != Chess.STATE_NOTDISCOVERED){
                    available[ii][j] = 1;
                }
                ii = i - 1;
                while(ii > 0 && x[ii][j].getArmyClass() == Chess.CHESS_NULL ){
                    available[ii][j] = 1;
                    ii--;
                }
                if(ii > 0 && eat(x[i][j].getArmyClass(),x[ii][j].getArmyClass()) && x[i][j].isEnemy()!= x[ii][j].isEnemy() && x[ii][j].getState() != Chess.STATE_NOTDISCOVERED){
                    available[ii][j] = 1;
                }
            }
            int jj = j + 1;
            while(jj < 5 && x[i][jj].getArmyClass() == Chess.CHESS_NULL ){
                available[i][jj] = 1;
                jj++;
            }
            if(jj < 5 && eat(x[i][j].getArmyClass(),x[i][jj].getArmyClass()) && x[i][j].isEnemy()!= x[i][jj].isEnemy() && x[i][jj].getState() != Chess.STATE_NOTDISCOVERED){
                available[i][jj] = 1;
            }
            jj = j - 1;
            while(jj>= 0 && x[i][jj].getArmyClass() == Chess.CHESS_NULL ){
                available[i][jj] = 1;
                jj--;
            }
            if(jj >= 0 && eat(x[i][j].getArmyClass(),x[i][jj].getArmyClass()) && x[i][j].isEnemy()!= x[i][jj].isEnemy() && x[i][jj].getState() != Chess.STATE_NOTDISCOVERED){
                available[i][jj] = 1;
            }
        }else if (i == 0 || i == 11){           // chess position situation: 1rd row or 12th row
            if(x[i==0?i+1:i-1][j].getArmyClass() == Chess.CHESS_NULL || (eat(x[i][j].getArmyClass(), x[i==0?i+1:i-1][j].getArmyClass()) && x[i][j].isEnemy()!= x[i==0?i+1:i-1][j].isEnemy() && x[i==0?i+1:i-1][j].getState() != Chess.STATE_NOTDISCOVERED)){          //no chess or chess can be eaten
                available[i==0?i+1:i-1][j] = 1;
            }
            if(j == 0 || j == 4){
                if(x[i][j==0?j+1:j-1].getArmyClass() == Chess.CHESS_NULL || (eat(x[i][j].getArmyClass(), x[i][j==0?j+1:j-1].getArmyClass()) && x[i][j].isEnemy()!= x[i][j==0?j+1:j-1].isEnemy() && x[i][j==0?j+1:j-1].getState() != Chess.STATE_NOTDISCOVERED)){          //no chess or chess can be eaten
                    available[i][j==0?j+1:j-1] = 1;
                }
            }else {
                if(x[i][j-1].getArmyClass() == Chess.CHESS_NULL || (eat(x[i][j].getArmyClass(), x[i][j-1].getArmyClass()) && x[i][j].isEnemy()!= x[i][j-1].isEnemy() && x[i][j-1].getState() != Chess.STATE_NOTDISCOVERED)){          //no chess or chess can be eaten
                    available[i][j-1] = 1;
                }
                if(x[i][j+1].getArmyClass() == Chess.CHESS_NULL || (eat(x[i][j].getArmyClass(), x[i][j+1].getArmyClass()) && x[i][j].isEnemy()!= x[i][j+1].isEnemy() && x[i][j+1].getState() != Chess.STATE_NOTDISCOVERED)){          //no chess or chess can be eaten
                    available[i][j+1] = 1;
                }
            }
        }
        return available;
    }

    private void specSapperPath(int[][] available,Chess[][] x, int i, int j ,boolean enemy){
        if(j == 0 || j == 4 || j == 2){
            if(((i-1 > 0 && j != 2 && i < 11) || (j == 2 && i == 6))&& available[i-1][j] != 1 && x[i-1][j].getArmyClass() == Chess.CHESS_NULL){     //go up
                available[i-1][j] = 1;
                specSapperPath(available,x,i-1,j,enemy);
            }else if(((i-1 > 0 && j != 2 && i < 11) || (j == 2 && i == 6)) && eat(Chess.CHESS_SAPPER,x[i-1][j].getArmyClass()) && enemy != x[i-1][j].isEnemy() && x[i-1][j].getState() != Chess.STATE_NOTDISCOVERED){
                available[i-1][j] = 1;
            }
            if(((i+1 < 11 && j != 2 && i > 0) || (j == 2 && i == 5))&& available[i+1][j] != 1 && x[i+1][j].getArmyClass() == Chess.CHESS_NULL)    {     //go down
                available[i+1][j] = 1;
                specSapperPath(available,x,i+1,j,enemy);
            }else if(((i+1 < 11 && j != 2 && i > 0) || (j == 2 && i == 5)) && eat(Chess.CHESS_SAPPER,x[i+1][j].getArmyClass()) && enemy != x[i+1][j].isEnemy() && x[i+1][j].getState() != Chess.STATE_NOTDISCOVERED){
                available[i+1][j] = 1;
            }
        }
        if(i == 1 || i == 5 || i == 6 || i == 10){
            if(j-1 >= 0 && available[i][j-1] != 1 && x[i][j-1].getArmyClass() == Chess.CHESS_NULL)    {     //go left
                available[i][j-1] = 1;
                specSapperPath(available,x,i,j-1,enemy);
            }else if(j-1 >= 0 && eat(Chess.CHESS_SAPPER,x[i][j-1].getArmyClass()) && enemy != x[i][j-1].isEnemy() && x[i][j-1].getState() != Chess.STATE_NOTDISCOVERED){
                available[i][j-1] = 1;
            }
            if(j+1 <= 4 && available[i][j+1] != 1 && x[i][j+1].getArmyClass() == Chess.CHESS_NULL)    {     //go right
                available[i][j+1] = 1;
                specSapperPath(available,x,i,j+1,enemy);
            }else if(j+1 <= 4 && eat(Chess.CHESS_SAPPER,x[i][j+1].getArmyClass()) && enemy != x[i][j+1].isEnemy() && x[i][j+1].getState() != Chess.STATE_NOTDISCOVERED){
                available[i][j+1] = 1;
            }
        }
    }

    private boolean eat(int x, int y){
        if((x<=y || x == Chess.CHESS_BOMB)&& y <= Chess.CHESS_SAPPER){
            return true;
        }else if(y == Chess.CHESS_MINE && (x == Chess.CHESS_SAPPER || x== Chess.CHESS_BOMB)){
            return true;
        }else if(y == Chess.CHESS_BOMB && (x != Chess.CHESS_MINE && x!= Chess.CHESS_FLAG)){
            return true;
        }else if(y == Chess.CHESS_FLAG && chessStatics[isDownBlue?whoIsEnemy:1-whoIsEnemy][Chess.CHESS_MINE][Chess.STATE_NOTDISCOVERED] == 0 && chessStatics[isDownBlue?whoIsEnemy:1-whoIsEnemy][Chess.CHESS_MINE][Chess.STATE_ALIVE] == 0 && x != Chess.CHESS_MINE && x!= Chess.CHESS_FLAG ){
            return true;
        }
        return false;
    }

    private OnGameEndListener _listener;

    public TurnchessOnlineGameView setOnGameEndListener(OnGameEndListener listener) {
        _listener = listener;
        return this;
    }

    public interface OnGameEndListener{
        void onGameEnd(int status);
    }

}

