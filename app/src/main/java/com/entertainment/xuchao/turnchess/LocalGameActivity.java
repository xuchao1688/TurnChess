package com.entertainment.xuchao.turnchess;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by Administrator1 on 2017/3/25.
 */

public class LocalGameActivity extends AppCompatActivity {
    private TurnchessLocalGameView turnchessLocalGameView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        turnchessLocalGameView = new TurnchessLocalGameView(this.getBaseContext());
        turnchessLocalGameView.setOnGameEndListener(new TurnchessLocalGameView.OnGameEndListener() {
            @Override
            public void onGameEnd(int status) {
                AlertDialog.Builder alertdialogBuilder = new AlertDialog.Builder(LocalGameActivity.this);
                alertdialogBuilder.setTitle(R.string.game_over)
                        .setMessage(status == 1 ? R.string.blue_win : R.string.red_win)
                        .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                finish();
                            }
                        });
                AlertDialog alertDialog = alertdialogBuilder.create();
                alertDialog.setCanceledOnTouchOutside(false);
                alertDialog.show();
            }
        });
        setContentView(turnchessLocalGameView);
    }
}
