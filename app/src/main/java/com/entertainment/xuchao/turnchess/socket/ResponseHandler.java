package com.entertainment.xuchao.turnchess.socket;

import entity.ResponseObject;

import java.io.ObjectInputStream;

/**
 * Created by Administrator1 on 2017/3/26.
 */

public class ResponseHandler {
    private ObjectInputStream ois;
    private ResponseObject resObject;
    private static ResponseHandler resHdler = null;
    public static ResponseHandler getResHdler(){
        if(resHdler == null)
            resHdler = new ResponseHandler();
        return  resHdler;
    }

    public void readMessage(){
        try{
            resObject = (ResponseObject) ois.readObject();
            _ResponseListener.doevent(resObject.getResType());
        }catch (Exception e){
            e.printStackTrace();
            //_ErrorListener.errorevent();
        }
    }
    public void setObjectInputStream(ObjectInputStream ois){
        this.ois = ois;
    }

    public ResponseObject getResObject(){
        return resObject;
    }
    private ResponseListener _ResponseListener = null;

    public ResponseHandler setResponseListener(ResponseListener responseListener){
        _ResponseListener = responseListener;
        return getResHdler();
    }
    public interface ResponseListener{
        public void doevent(int status);
    }

    private ResponseHandler.ErrorListener _ErrorListener = null;

    public ResponseHandler setErrorListener(ErrorListener errorListener){
        _ErrorListener = errorListener;
        return ResponseHandler.getResHdler();
    }
    public interface ErrorListener{
        public void errorevent();
    }
}
