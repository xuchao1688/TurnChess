package com.entertainment.xuchao.turnchess;
/**
 * Created by Administrator1 on 2017/4/21.
 */
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;

import android.app.DownloadManager;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;

import android.os.AsyncTask;

import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatDialog;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.graphics.drawable.RippleDrawable;
import android.util.ArrayMap;

import com.entertainment.xuchao.turnchess.socket.*;
import entity.*;

/**
 * A modify account screen that offers login via account/password.
 */
public class OnlineGameModifyAccountActivity extends AppCompatActivity {

    /**
     * Keep track of the modify account task to ensure we can cancel it if requested.
     */
    private UserModifyAccountTask mModifyAccountTask = null;

    // UI references.
    private EditText mAccountView;
    private EditText mOldPasswordView;
    private EditText mNewPasswordView;
    private EditText mNewConfirmPasswordView;
    private View mProgressView;
    private View mModifyAccountFormView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modifyaccount);
        // Set up the modify account form.
        mAccountView = (EditText) findViewById(R.id.account);

        mOldPasswordView = (EditText) findViewById(R.id.old_password);
        mNewPasswordView = (EditText) findViewById(R.id.new_password);
        mNewConfirmPasswordView = (EditText) findViewById(R.id.new_confirm_password);
        mNewConfirmPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.modify_account || id == EditorInfo.IME_NULL) {
                    attemptModifyAccount();
                    return true;
                }
                return false;
            }
        });

        Button mAccountModifyAccountButton = (Button) findViewById(R.id.account_modify_account_button);
        mAccountModifyAccountButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptModifyAccount();
            }
        });

        Button mBackButton = (Button) findViewById(R.id.account_modifyaccount_back_button);
        mBackButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mModifyAccountFormView = findViewById(R.id.modify_account_form);
        mProgressView = findViewById(R.id.modify_account_progress);
    }

    /**
     * Attempts to modify the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptModifyAccount() {
        if (mModifyAccountTask!= null) {
            return;
        }

        // Reset errors.
        mAccountView.setError(null);
        mOldPasswordView.setError(null);
        mNewPasswordView.setError(null);
        mNewConfirmPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String account = mAccountView.getText().toString();
        String oldPassword = mOldPasswordView.getText().toString();
        String newPassword = mNewPasswordView.getText().toString();
        String newConfirmPassword = mNewConfirmPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if(!newConfirmPassword.equals(newPassword)){
            mNewConfirmPasswordView.setError(getString(R.string.error_invalid_confirmPassword));
            focusView = mNewConfirmPasswordView;
            cancel = true;
        }else if(TextUtils.isEmpty(newConfirmPassword)){
            mNewConfirmPasswordView.setError(getString(R.string.error_field_required));
            focusView = mNewConfirmPasswordView;
            cancel = true;
        }

        if (!TextUtils.isEmpty(newPassword) && !isPasswordValid(newPassword)) {
            mNewPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mNewPasswordView;
            cancel = true;
        }else if(TextUtils.isEmpty(newPassword)){
            mNewPasswordView.setError(getString(R.string.error_field_required));
            focusView = mNewPasswordView;
            cancel = true;
        }

        if (!TextUtils.isEmpty(oldPassword) && !isPasswordValid(oldPassword)) {
            mOldPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mOldPasswordView;
            cancel = true;
        }else if(TextUtils.isEmpty(oldPassword)){
            mOldPasswordView.setError(getString(R.string.error_field_required));
            focusView = mOldPasswordView;
            cancel = true;
        }

        // Check for a valid account.
        if (TextUtils.isEmpty(account)) {
            mAccountView.setError(getString(R.string.error_field_required));
            focusView = mAccountView;
            cancel = true;
        } else if (!isAccountValid(account)) {
            mAccountView.setError(getString(R.string.error_invalid_account));
            focusView = mAccountView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            showProgress(true);
            mModifyAccountTask = new UserModifyAccountTask(account, oldPassword, newPassword);
            mModifyAccountTask.execute((Void) null);
        }
    }

    private boolean isAccountValid(String account) {
        //TODO: Replace this with your own logic
        boolean isDigit = false;
        boolean isLetter = false;
        for(int i = 0; i < account.length();i++){
            if(Character.isDigit(account.charAt(i))){
                isDigit = true;
            }else if (Character.isLetter(account.charAt(i))){
                isLetter = true;
            }
        }
        return isDigit && isLetter && (account.length() >= 4);
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        boolean isDigit = false;
        boolean isLetter = false;
        for(int i = 0; i < password.length();i++){
            if(Character.isDigit(password.charAt(i))){
                isDigit = true;
            }else if (Character.isLetter(password.charAt(i))){
                isLetter = true;
            }
        }
        return isDigit && isLetter && (password.length() >= 4);
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
            mModifyAccountFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mModifyAccountFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mModifyAccountFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mModifyAccountFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    /**
     * Represents an asynchronous modifying account task used to authenticate
     * the user.
     */
    public class UserModifyAccountTask extends AsyncTask<Void, Void, Boolean> {

        private final String mAccount;
        private final String mOldPassword;
        private final String mNewPassword;
        private boolean successModifyAccount;

        UserModifyAccountTask(String account, String oldPassword, String newPassword) {
            mAccount = account;
            mOldPassword = oldPassword;
            mNewPassword = newPassword;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            // TODO: attempt authentication through a network service.
            InitializeConnection.getInitConn();
            ResponseHandler.getResHdler().setResponseListener(new ResponseHandler.ResponseListener() {  // read the result of modify account
                @Override
                public void doevent(int status) {
                    if(status == ResponseObject.RES_MODIFYACCOUNT){
                        successModifyAccount = ((ModifyAccountInfoRes)ResponseHandler.getResHdler().getResObject().getResBody()).isSuccess();
                    }
                }
            });
            RequestHandler.getReqHdler().sendMessage(new RequestObject(RequestObject.REQ_MODIFYACCOUNT, new ModifyAccountInfo(mAccount, mOldPassword, mNewPassword))); //send modify account info to server
            ResponseHandler.getResHdler().readMessage();
            return true;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mModifyAccountTask = null;
            showProgress(false);
            if(success && successModifyAccount){
                AlertDialog.Builder alertdialogBuilder = new AlertDialog.Builder(OnlineGameModifyAccountActivity.this);
                alertdialogBuilder.setTitle(R.string.congratulation)
                        .setMessage(R.string.success_modify_account)
                        .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                finish();
                            }
                        });
                AlertDialog alertDialog = alertdialogBuilder.create();
                alertDialog.setCanceledOnTouchOutside(false);
                alertDialog.show();
            }else{
                mAccountView.setError(getString(R.string.error_incorrect_password));
                mAccountView.requestFocus();
            }
        }

        @Override
        protected void onCancelled() {
            mModifyAccountTask = null;
            showProgress(false);
        }
    }
}

