package com.entertainment.xuchao.turnchess;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.entertainment.xuchao.turnchess.socket.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import entity.*;

/**
 * Created by Administrator1 on 2017/4/19.
 */

public class MessageBoxActivity extends AppCompatActivity{
    private Button backToMainBtn;
    private ListView messageBoxListView;
    private List<Map<String, Object>> mData;
    private MessageBoxListAdapter mAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_messagebox);
        initView();
    }
    private void initView(){
        InitializeConnection.getInitConn();
        mData = new ArrayList<Map<String,Object>>();
        ResponseHandler.getResHdler().setResponseListener(new ResponseHandler.ResponseListener() {
            @Override
            public void doevent(int status) {
                if(status == ResponseObject.RES_LOADMESSAGEBOX){
//                    runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//
//                        }
//                    });
                    mData.clear();
                    System.out.println((LoadMessageboxInfoRes)ResponseHandler.getResHdler().getResObject().getResBody());
                    int[] userlist = ((LoadMessageboxInfoRes)ResponseHandler.getResHdler().getResObject().getResBody()).getUserlist();
                    String[] username = ((LoadMessageboxInfoRes)ResponseHandler.getResHdler().getResObject().getResBody()).getUsername();
                    for(int i = 0; i < userlist.length; i++){
                        Map<String,Object> map = new HashMap<String,Object>();
                        map.put("username",username[i]);
                        map.put("id",userlist[i]);
                        mData.add(map);
                    }
                }
            }
        });
        backToMainBtn = (Button)findViewById(R.id.backToMainBtn);
        backToMainBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        messageBoxListView = (ListView)findViewById(R.id.messageBoxList);
        new Thread(new Runnable() {
            @Override
            public void run() {
                RequestHandler.getReqHdler().sendMessage(new RequestObject(RequestObject.REQ_LOADMESSAGEBOX,null));
                ResponseHandler.getResHdler().readMessage();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mAdapter.notifyDataSetChanged();
                    }
                });
            }
        }).start();
        mAdapter = new MessageBoxListAdapter(MessageBoxActivity.this);
        messageBoxListView.setAdapter(mAdapter);
    }

    public final class ViewHolder{
        public TextView friendlist_username;
        public Button acceptAddNewfriendBtn;
        public Button refuseAddNewfriendBtn;
    }

    public class MessageBoxListAdapter extends BaseAdapter{
        private LayoutInflater mInflater;
        public MessageBoxListAdapter(Context context){
            this.mInflater = LayoutInflater.from(context);
        }

        @Override
        public int getCount(){
            return mData.size();
        }

        @Override
        public Object getItem(int arg0) {
            return null;
        }

        @Override
        public long getItemId(int arg0) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent){
            ViewHolder holder = null;
            if(convertView == null){
                holder = new ViewHolder();
                convertView = mInflater.inflate(R.layout.messagebox,null);
                holder.friendlist_username = (TextView)convertView.findViewById(R.id.friendlist_username);
                holder.acceptAddNewfriendBtn = (Button) convertView.findViewById(R.id.acceptAddNewfriendBtn);
                holder.refuseAddNewfriendBtn = (Button) convertView.findViewById(R.id.refuseAddNewfriendBtn);
                convertView.setTag(holder);
            }else {
                holder = (ViewHolder)convertView.getTag();
            }
            holder.friendlist_username.setText((String)((String)(mData.get(position).get("username"))+"  "+(String)getString(R.string.want_to_add_newfriend)));
            final String text = mData.get(position).get("username").toString();
            holder.acceptAddNewfriendBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            RequestHandler.getReqHdler().sendMessage(new RequestObject(RequestObject.REQ_CONFIRMADDNEWFRIEND,new NewFriendInfo(text)));
                            ResponseHandler.getResHdler().readMessage();
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    mAdapter.notifyDataSetChanged();
                                }
                            });
                        }
                    }).start();
                }
            });
            holder.refuseAddNewfriendBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            RequestHandler.getReqHdler().sendMessage(new RequestObject(RequestObject.REQ_REFUSEADDNEWFRIEND,new NewFriendInfo(text)));
                            ResponseHandler.getResHdler().readMessage();
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    mAdapter.notifyDataSetChanged();
                                }
                            });
                        }
                    }).start();
                }
            });
            return convertView;
        }
    }
}
