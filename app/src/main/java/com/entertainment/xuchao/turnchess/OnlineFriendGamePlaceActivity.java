package com.entertainment.xuchao.turnchess;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.entertainment.xuchao.turnchess.socket.RequestHandler;
import com.entertainment.xuchao.turnchess.socket.ResponseHandler;

import entity.FriendGameInfo;
import entity.FriendGameInfoRes;
import entity.RequestObject;
import entity.ResponseObject;

/**
 * Created by Administrator1 on 2017/4/23.
 */

public class OnlineFriendGamePlaceActivity extends AppCompatActivity {
    private View mProgressView;
    private boolean busy;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friendgameplace);
        initView();
    }
    private void initView(){
        busy = true;
        mProgressView = findViewById(R.id.friendgameplace_progress);
        showProgress(true);
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (busy){
                    ResponseHandler.getResHdler().readMessage();
                }
            }
        }).start();
        ResponseHandler.getResHdler().setResponseListener(new ResponseHandler.ResponseListener() {
            @Override
            public void doevent(int status) {
                if(status == ResponseObject.RES_FRIENDGAME){
                    final FriendGameInfoRes friendGameInfoRes = (FriendGameInfoRes)ResponseHandler.getResHdler().getResObject().getResBody();
                    if(friendGameInfoRes.getGameType() == FriendGameInfoRes.GAME_INVITE){
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                AlertDialog.Builder alertdialogBuilder = new AlertDialog.Builder(OnlineFriendGamePlaceActivity.this);
                                alertdialogBuilder.setTitle(R.string.friend_game_invite)
                                        .setMessage((String)(friendGameInfoRes.getAccount()+" "+ getString(R.string.invite_you_to_game)))
                                        .setNegativeButton(getString(R.string.refuse_friend_game), new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                new Thread(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        FriendGameInfo friendGameInfo = new FriendGameInfo(FriendGameInfo.GAME_CACNCEL);
                                                        friendGameInfo.setUid(friendGameInfoRes.getUid());
                                                        RequestHandler.getReqHdler().sendMessage(new RequestObject(RequestObject.REQ_FRIENDGAME,friendGameInfo));
                                                    }
                                                }).start();
                                            }
                                        })
                                        .setPositiveButton(getString(R.string.acceept_add_newfriend), new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                final FriendGameInfo friendGameInfo = new FriendGameInfo(FriendGameInfo.GAME_CONFIRMREADY);
                                                friendGameInfo.setUid(friendGameInfoRes.getUid());
                                                ResponseHandler.getResHdler().setResponseListener(new ResponseHandler.ResponseListener() {
                                                    @Override
                                                    public void doevent(int status) {
                                                        if(status == ResponseObject.RES_FRIENDGAME){
                                                            Intent intent = new Intent(OnlineFriendGamePlaceActivity.this,OnlineGameActivity.class);
                                                            startActivity(intent);
                                                            finish();
                                                        }
                                                    }
                                                });
                                                busy = false;
                                                new Thread(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        RequestHandler.getReqHdler().sendMessage(new RequestObject(RequestObject.REQ_FRIENDGAME,friendGameInfo));
                                                    }
                                                }).start();
                                            }
                                        });
                                AlertDialog alertDialog = alertdialogBuilder.create();
                                alertDialog.setCanceledOnTouchOutside(false);
                                alertDialog.show();
                            }
                        });
                    }
                }
            }
        });
    }
    @Override
    public void onBackPressed() {
        busy = false;
        super.onBackPressed();
    }
    /**
     * Shows the progress UI and hides the online main form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
        }
    }
}
