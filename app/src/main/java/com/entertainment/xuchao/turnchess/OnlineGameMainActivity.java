package com.entertainment.xuchao.turnchess;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.entertainment.xuchao.turnchess.socket.*;

import java.text.DecimalFormat;

import entity.*;

/**
 * Created by Administrator1 on 2017/4/19.
 */

public class OnlineGameMainActivity extends AppCompatActivity {
    private Button matchGameStartBtn;
    private Button friendGameStartBtn;
    private Button messageBoxBtn;
    private Button selfUserInfoBtn;
    private Button operationHelpBtn;
    private Button applicationHelpBtn;
    private View mProgressView;
    private View mOnlineMainFormView;
    private boolean busy;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_onlinemain);
        busy = false;
        initView();
    }
    private void initView(){
        matchGameStartBtn = (Button)findViewById(R.id.matchGameStartBtn);
        matchGameStartBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                busy = true;
                showProgress(true);
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        ResponseHandler.getResHdler().setResponseListener(new ResponseHandler.ResponseListener() {
                            @Override
                            public void doevent(int status) {
                                if(status == ResponseObject.RES_MATCHGAME){
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            showProgress(false);
                                            busy = false;
                                        }
                                    });
                                    Intent intent = new Intent(OnlineGameMainActivity.this,OnlineGameActivity.class);
                                    startActivity(intent);
                                }
                            }
                        });
                        RequestHandler.getReqHdler().sendMessage(new RequestObject(RequestObject.REQ_MATCHGAME,new MatchGameInfo(MatchGameInfo.GAME_READY)));
                        ResponseHandler.getResHdler().readMessage();
                    }
                }).start();
            }
        });
        friendGameStartBtn = (Button)findViewById(R.id.friendGameStartBtn);
        friendGameStartBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(OnlineGameMainActivity.this,OnlineFriendGameReadyActivity.class);
                startActivity(intent);
            }
        });
        messageBoxBtn = (Button)findViewById(R.id.messageBoxBtn);
        messageBoxBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(OnlineGameMainActivity.this,MessageBoxActivity.class);
                startActivity(intent);
            }
        });
        selfUserInfoBtn = (Button)findViewById(R.id.selfUserInfoBtn);
        selfUserInfoBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        RequestHandler.getReqHdler().sendMessage(new RequestObject(RequestObject.REQ_LOADUSERINFO,new LoadUserInfo(null,true)));
                        ResponseHandler.getResHdler().setResponseListener(new ResponseHandler.ResponseListener() {
                            @Override
                            public void doevent(int status) {
                                if(status == ResponseObject.RES_LOADUSERINFO){
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            LoadUserInfoRes loadUserInfoRes = (LoadUserInfoRes)ResponseHandler.getResHdler().getResObject().getResBody();
                                            DecimalFormat decimalFormat=new DecimalFormat(".00%");
                                            AlertDialog.Builder alertdialogBuilder = new AlertDialog.Builder(OnlineGameMainActivity.this);
                                            alertdialogBuilder.setTitle(R.string.self_userinfo)
                                                    .setMessage((String)(loadUserInfoRes.getUsername() + "\n" + getString(R.string.win)+
                                                            ":  " + loadUserInfoRes.getWin() + "\n" + getString(R.string.lose))+
                                                            ":  " + loadUserInfoRes.getLose() + "\n" + getString(R.string.win_rate)+
                                                            ":  " + decimalFormat.format(1.00f * loadUserInfoRes.getWin()/(loadUserInfoRes.getWin()+loadUserInfoRes.getLose())))
                                                    .setPositiveButton(getString(R.string.yes), null)
                                                    .setNegativeButton(getString(R.string.modify_password), new DialogInterface.OnClickListener() {
                                                        @Override
                                                        public void onClick(DialogInterface dialog, int which) {
                                                            Intent intent = new Intent(OnlineGameMainActivity.this, OnlineGameModifyAccountActivity.class);
                                                            startActivity(intent);
                                                        }
                                                    });
                                            AlertDialog alertDialog = alertdialogBuilder.create();
                                            alertDialog.setCanceledOnTouchOutside(false);
                                            alertDialog.show();
                                        }
                                    });
                                }
                            }
                        });
                        ResponseHandler.getResHdler().readMessage();
                    }
                }).start();
            }
        });
        operationHelpBtn = (Button)findViewById(R.id.operationHelpBtn);
        operationHelpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        RequestHandler.getReqHdler().sendMessage(new RequestObject(RequestObject.REQ_OPERATIONHELP,null));
                        ResponseHandler.getResHdler().setResponseListener(new ResponseHandler.ResponseListener() {
                            @Override
                            public void doevent(int status) {
                                if(status == ResponseObject.RES_OPERATIONHELP){
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            HelpInfoRes helpInfoRes = (HelpInfoRes) ResponseHandler.getResHdler().getResObject().getResBody();
                                            AlertDialog.Builder alertdialogBuilder = new AlertDialog.Builder(OnlineGameMainActivity.this);
                                            alertdialogBuilder.setTitle(R.string.operation_help)
                                                    .setMessage(helpInfoRes.getMessage())
                                                    .setPositiveButton(getString(R.string.yes), null);
                                            AlertDialog alertDialog = alertdialogBuilder.create();
                                            alertDialog.setCanceledOnTouchOutside(false);
                                            alertDialog.show();
                                        }
                                    });
                                }
                            }
                        });
                        ResponseHandler.getResHdler().readMessage();
                    }
                }).start();
            }
        });
        applicationHelpBtn = (Button)findViewById(R.id.applicationHelpBtn);
        applicationHelpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        RequestHandler.getReqHdler().sendMessage(new RequestObject(RequestObject.REQ_APPLICATIONHELP,null));
                        ResponseHandler.getResHdler().setResponseListener(new ResponseHandler.ResponseListener() {
                            @Override
                            public void doevent(int status) {
                                if(status == ResponseObject.RES_APPLICATIONHELP){
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            HelpInfoRes helpInfoRes = (HelpInfoRes) ResponseHandler.getResHdler().getResObject().getResBody();
                                            AlertDialog.Builder alertdialogBuilder = new AlertDialog.Builder(OnlineGameMainActivity.this);
                                            alertdialogBuilder.setTitle(R.string.application_help)
                                                    .setMessage(helpInfoRes.getMessage())
                                                    .setPositiveButton(getString(R.string.yes), null);
                                            AlertDialog alertDialog = alertdialogBuilder.create();
                                            alertDialog.setCanceledOnTouchOutside(false);
                                            alertDialog.show();
                                        }
                                    });
                                }
                            }
                        });
                        ResponseHandler.getResHdler().readMessage();
                    }
                }).start();
            }
        });
        mOnlineMainFormView = findViewById(R.id.onlinemain_form);
        mProgressView = findViewById(R.id.onlinegame_progress);
    }
    @Override
    public void onBackPressed(){
        if(busy){
            new Thread(new Runnable() {
                @Override
                public void run() {
                    RequestHandler.getReqHdler().sendMessage(new RequestObject(RequestObject.REQ_MATCHGAME,new MatchGameInfo(MatchGameInfo.GAME_CACNCEL)));
                }
            }).start();
        }
        super.onBackPressed();
    }
    /**
     * Shows the progress UI and hides the online main form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mOnlineMainFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mOnlineMainFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mOnlineMainFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mOnlineMainFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }
}
