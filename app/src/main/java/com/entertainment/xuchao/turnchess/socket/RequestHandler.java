package com.entertainment.xuchao.turnchess.socket;

/**
 * Created by Administrator1 on 2016/11/20.
 */

import entity.RequestObject;

import java.io.*;

public class RequestHandler {      //send request to server
    private ObjectOutputStream oos;
    private static RequestHandler reqHdler = null;
    public static RequestHandler getReqHdler(){
        if(reqHdler == null)
            reqHdler = new RequestHandler();
        return  reqHdler;
    }

    public void sendMessage(RequestObject reqObj){
        try{
            oos.writeObject(reqObj);
        }catch (Exception e){
            e.printStackTrace();
            //_ErrorListener.errorevent();
        }
    }
    public void setObjectOutputStream(ObjectOutputStream oos){
        this.oos = oos;
    }

    private ErrorListener _ErrorListener = null;

    public RequestHandler setErrorListener(ErrorListener errorListener){
        _ErrorListener = errorListener;
        return RequestHandler.getReqHdler();
    }
    public interface ErrorListener{
        public void errorevent();
    }

}
