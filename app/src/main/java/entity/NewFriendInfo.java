package entity;
import java.io.Serializable;

/**
 * Created by Administrator1 on 2016/11/27.
 */
public class NewFriendInfo implements Serializable {
    private String account;
    public NewFriendInfo(String name){
        account = name;
    }
    @Override
    public String toString(){
        return "account is " + account;
    }
    public String getUsername(){
        return account;
    }
}
