package entity;

import java.io.Serializable;

/**
 * Created by Administrator1 on 2017/4/21.
 */
public class ModifyAccountInfoRes implements Serializable {
    private String message;
    private boolean success;
    public ModifyAccountInfoRes(String mess, boolean succ){
        message = mess;
        success = succ;
    }
    @Override
    public String toString(){
        return "message is '" + message +"',"+ "result is '"+ success +"'.";
    }
    public String getMessage(){
        return message;
    }
    public boolean isSuccess(){
        return success;
    }
}
