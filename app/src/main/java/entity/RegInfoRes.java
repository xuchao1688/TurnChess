package entity;

import java.io.Serializable;

/**
 * Created by Administrator1 on 2017/3/26.
 */

public class RegInfoRes implements Serializable{
    private String message;
    private boolean success;
    public RegInfoRes(String mess, boolean succ){
        super();
        message = mess;
        success = succ;
    }
    @Override
    public String toString(){
        return "message is '" + message +"'; "+"if success in login: " + success;
    }
    public String getMessage(){
        return message;
    }
    public Boolean getSuccess(){
        return success;
    }
}
