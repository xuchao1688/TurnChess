package entity;

import java.io.Serializable;

/**
 * Created by Administrator1 on 2017/4/23.
 */
public class HelpInfoRes implements Serializable {
    private String message;
    public HelpInfoRes(String mess){
        message = mess;
    }
    @Override
    public String toString(){
        return "help information";
    }
    public String getMessage(){
        return message;
    }
}

