package entity;

import java.io.Serializable;

/**
 * Created by Administrator1 on 2017/4/21.
 */
public class LoadUserInfoRes implements Serializable {
    private String account;
    private int win;
    private int lose;

    public LoadUserInfoRes(String name, int wi, int lo){
        account = name;
        win = wi;
        lose = lo;
    }
    @Override
    public String toString(){
        return "account is " + account;
    }
    public String getUsername(){
        return account;
    }
    public int getWin(){
        return win;
    }
    public int getLose(){
        return lose;
    }
}
