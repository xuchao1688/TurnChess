package entity;

import java.io.Serializable;

/**
 * Created by Administrator1 on 2017/4/22.
 */
public class FriendGameInfo implements Serializable {
    public static final int GAME_READY = 1;
    public static final int GAME_ON_TURN = 2;
    public static final int GAME_ON_MOVE = 3;
    public static final int GAME_END = 4;
    public static final int GAME_CACNCEL = 5;
    public static final int GAME_CONFIRMREADY = 6;
    private int uid;
    private boolean selfWin;
    private int oldI,oldJ,newI,newJ;
    private int gameType;
    public FriendGameInfo(int type){
        gameType = type;
    }
    @Override
    public String toString(){
        return "gameType: "+ gameType;
    }
    public int getGameType(){
        return gameType;
    }
    public int getUid(){
        return uid;
    }
    public void setUid(int id){
        uid = id;
    }
    public boolean isSelfWin(){
        return selfWin;
    }
    public void setSelfWin(boolean win){
        selfWin = win;
    }

    public int getOldI(){
        return oldI;
    }
    public void setOldI(int i){
        oldI = i;
    }

    public int getOldJ(){
        return oldJ;
    }
    public void setOldJ(int j){
        oldJ = j;
    }

    public int getNewI(){
        return newI;
    }
    public void setNewI(int i){
        newI = i;
    }

    public int getNewJ(){
        return newJ;
    }
    public void setNewJ(int j){
        newJ = j;
    }

}
